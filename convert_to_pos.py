#!/usr/bin/env python3

#This function converts the files prensent in the folders created in the split.py program to POS-tag sentence files.

#Use this program by first calling python3, then convert_to_pos.py, provide the number of sentences and specify whether to run on training or test data.
#Example: python3 convert_to_pos.py 20 training

import sys
import glob
from nltk import pos_tag
from nltk.tokenize import wordpunct_tokenize

def main(argv):
	'''Convert textual files to files containing their respective POS tags'''
	no_of_sentences = argv[1] #Provided (via argv) number of sentences
	test_or_training = argv[2] #Provided test or train set

	try: #Change which folder needs to checked depending on provided argument test or training
		if test_or_training == "training":
			google = "{} {} {}{}".format("google", no_of_sentences, "sent",  "/*.txt")
			sdl = "{} {} {}{}".format("sdl", no_of_sentences, "sent", "/*.txt")
		elif test_or_training == "test":	
			google = "{} {} {}{}".format("test google", no_of_sentences, "sent", "/*.txt")
			sdl = "{} {} {}{}".format("test sdl", no_of_sentences, "sent", "/*.txt")

		files_google = glob.glob(google) #Look through the google directory described above and put files in a list
		files_sdl = glob.glob(sdl) #Look through the sdl directory described above and put files in a list

		total_google = len(files_google)
		for i, path in enumerate(files_google): #Iterate over the google files
			with open(path, 'r') as fh:
				if test_or_training == "training":
					google_pos = "{} {} {}".format("google", no_of_sentences, "sent pos/")
					with open("{}{}{}".format(google_pos, i, ".pos"), "w") as fh2:
						for line in fh:
							print(' '.join([pos for _, pos in pos_tag(wordpunct_tokenize(line))]), file=fh2) #get the pos tag for a word and add it to the sentence
				elif test_or_training == "test":
					google_pos = "{} {} {}".format("test google", no_of_sentences, "sent pos/")
					with open("{}{}{}".format(google_pos, i, ".pos"), "w") as fh2:
						for line in fh:
							print(' '.join([pos for _, pos in pos_tag(wordpunct_tokenize(line))]), file=fh2) #get the pos tag for a word and add it to the sentence
			if i%100 == 0: #Function to see roughly how long you have to wait for the conversion
				print('{}/{}'.format(i, total_google))

		total_sdl = len(files_sdl)
		for i, path in enumerate(files_sdl): #Iterate ove the sdl files
			with open(path, 'r') as fh:
				if test_or_training == "training":
					sdl_pos = "{} {} {}".format("sdl", no_of_sentences, "sent pos/")
					with open("{}{}{}".format(sdl_pos, i, ".pos"), "w") as fh2:
						for line in fh:
							print(' '.join([pos for _, pos in pos_tag(wordpunct_tokenize(line))]), file=fh2) #get the pos tag for a word and add it to the sentence
				elif test_or_training == "test":
					sdl_pos = "{} {} {}".format("test sdl", no_of_sentences, "sent pos/")
					with open("{}{}{}".format(sdl_pos, i, ".pos"), "w") as fh2:
						for line in fh:
							print(' '.join([pos for _, pos in pos_tag(wordpunct_tokenize(line))]), file=fh2) #get the pos tag for a word and add it to the sentence
			if i%100 == 0: #Function to see roughly how long you have to wait for the conversion
				print('{}/{}'.format(i, total_sdl))

	except NameError: #When a wrong word is presented as argv show the following error
		print("{} \n {}".format("Please input the right paramaters", "Example: python3 pipe.py 20 training"))

if __name__ == "__main__":
	main(sys.argv)