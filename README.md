# Bachelor thesis

In this project I wanted to test whether it was possible to create a classifier which could classify whether a given text was translated from English to Dutch by the Google Translation Toolkit or the translator provided by SDL. Books were download from the Gutenberg project and translated by either of these translation services. All of the books were combined into two files, one for each label (Google and SDL). These files were then split in documents containing either 1, 5, 10 or 20 sentences to see which amount of data would lead to the best performance.  Different vectorizers were compared using the F-score on the 5 sentence length files. Different classifiers were compared using one-way ANOVA. Different features and number of sentences per document were then compared to choose the optimal model per number of sentences per document. The text in the files was also converted to Part of Speech (POS) tags, on which all models and their number of sentences would also be compared. The best performing models would be run on the test set containing yet unseen files of sentence length 1, 5, 10 or 20. 

## Getting Started

Download the repository under the tab "Download" in the following link: https://bitbucket.org/CorbenPoot/bachelor_scriptie/src.

### Prerequisites
For these programs to run, you need to use at least python version 3.5.2. 
The following python3 packages should also be installed for the program to be able to run:

Install NLTK: sudo pip3 install -U nltk
Install Numpy (optional): run sudo pip3 install -U numpy
Install SKlearn: sudo pip3 install -U scikit-learn

Furthermore, you need R and RStudio to run the statistical tests.

### Short explanation of repository build up
The following files are available:
```
- pipe.py: The classification program
- statistical_pipe.py: Program used for measuring performance between different classifiers
- split.py: Used for splitting the google and sdl data into files containing a variable amount of sentences
- convert_to_pos.py: Convert the files created by split.py to POS-tag files
```

The following folders are available:
```
classifier comparison: Contains all the files, including the R-script, that were used for comparing the different classifiers
google: Contains the files translated from the "original books" folder by Google Translate and the file containing the concatenated books
google_x_sent(_pos): x stands for the amount of sentences created when the files from google were split, and pos is added whenever the files contain the POS-tags
original books: Contains the original books from the Gutenberg Project in plain text format
results: Contains the results in txt format for the training and development set for all the different models, named accordingly within the folder
results pos: Same as results, but for the POS-tag files
sdl: Contains the files translated from the "original books" folder by SDL Translate and the file containing the concatenated books
sdl_x_sent(_pos): x stands for the amount of sentences created when the files from sdl were split, and pos is added whenever the files contain the POS-tags
test original books: Contained the test set of books, same format as orignal books
test_x: There is also a test version of the 6 folder types described above, but for the different models, only the best were chosen
vectorizer comparison: Contains the two resulting files for comparing the different vectorizers, only the 5_sent file is used in the thesis
```



## Preprocessing
All files are already present in the different folders of this repository but for this explanation we will assume that they are not.
The following steps should be taken to prepare the data:

```
1. Use the program split.py. This program splits the data translated by either Google Translate or SDL Translate into different amounts of sentences (whichever provided). To run this program to split the Google Translated training and development files into 20 sentences you have to run the following command: python3 split.py 20 training google. To do this for 1, 5 or 10 sentences you have to change the number 20 to one of these numbers. To change the data to the test data, you will have to change the "training" argument to "test" and to change the files to the SDL translated files you have to change "google" to "sdl". 

2. (optional) If you want to convert the files created in step 1 to POS-tag files, you can use the program convert_to_pos.py. It converts the files to POS-tag files and writes them to their respective folders. To convert the 20 sentence files from the training data you have to run the following command: python3 convert_to_pos.py 20 training. Again the parameters can be changed like described above, they will have the same effect.

3. (optional) To create the data necessary for the statistical test, you use the statistical_pipe.py program. This program splits balances the 1 sentence per document files for the Google Translate and the SDL Translate files and divides them into 100 equal parts. These parts are passed on to the classifier which returns the F-score of these parts. To run this program, use the following command: python3 statistical_pipe.py training text. The parameters can be changed like described above. To run this for the other classifiers, the standard is LinearSVC, change the variable LinearSVC to either BernoulliNB
or MultinomialNB. You can write the output to a text file by adding > "name_this_however_you_want.txt" to the program call.
```

### Running the python Program

To run the program, follow this explanation:

```
To get the results of the LinearSVC classifier for 20 textual sentences from the test data, using a tfidfvectorizer to convert the data to numeric usable data for the classifier, run the following code: python3 pipe.py 20 test text. The number 20 can be changed to 1, 5 or 10 according to how many sentences you want to use per document, the "test" argument can be changed to "training" to run the classifier on the training data, and finally, the "text" argument can be changed to pos to run the program on the POS-tag files. Within the program the features of the vectorizer can be added, you can change binary=True to binary=False and ngram_range=(1,2) to run the program for unigrams and bigrams, but of course you can change these numbers to any number of n-grams you want to use. The program will return the number of features the classifier used, the classification report containing the precision, recall, F-score and amount of documents used per label. The classification report also contains the averaged precision, recall and F-score and the total amount of documents used. Furthermore the output consists of the most informative features for each of the labels.
```

When running the following command, the output will look similar to this, depending on the variables changed:

python3 pipe.py 20 test text:
```
Number of features used: 693886
        google                          sdl


        -3.3707 de zijne                3.7015  uw
        -3.2309 jij                     3.3082  vervolgens
        -3.1137 zijne                   3.0076  aljosja
        -3.0465 daar                    2.6076  wij
        -2.6722 geweest                 2.2416  één
        -2.5497 alyosha                 2.1008  zeer
        -2.3133 binnen                  2.0913  één van
        -2.2704 toe                     1.9817  it
        -1.9823 het niet                1.8399  collega
        -1.8950 af                      1.7647  zij


             precision    recall  f1-score   support

     google       0.96      0.94      0.95      3608
        sdl       0.94      0.96      0.95      3715

avg / total       0.95      0.95      0.95      7323
```

### Running the R-script
To be able to run the R-script, you need to go into the folder called "classifier comparison" and open the Anova_f_scores.R file with RStudio. You select the (part of) the code you want to run and then
use the "run" button to see the results. 

