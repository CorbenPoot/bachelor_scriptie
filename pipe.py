#!/usr/bin/env python3

#This program is provided with parameters which tell it how many sentences per file it has to use, whether to use training or test data, whether to use text or POS-tag files and then
#the corresponding files will be classified by this program. Given the input it returns the precision, recall, F-score and amount of documents used per label for the classifier. 
#It also shows the averaged precision, recall and F-score for all of the labels and the total amount of documents used. 
#Furthermore, it shows the most informative features per label.

#How to use: Call python3, call the program pipe.py, give no. of sentences, training or test data, text or POS-tag files.
#Example: python3 pipe.py 20 training text

import sys
import glob
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.svm import LinearSVC
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import classification_report

np.random.seed(0) #Makes the random numbers predictable so that each time the same intances are used

def show_most_informative_features(vectorizer, clf, n=10):
	'''This function shows the most informative features per label, and the amount of features used. 
	This code was retrieved and adapted from:  
	https://stackoverflow.com/questions/11116697/how-to-get-most-informative-features-for-scikit-learn-classifiers#comment27651495_11116960'''

	feature_names = vectorizer.get_feature_names() #Gets the feature names
	print("{} {}".format("Number of features used:", len(feature_names))) #Prints the number of features used by the classifier
	coefs_with_fns = sorted(zip(clf.coef_[0], feature_names)) #Get the coefficients and their feature names
	top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1]) #Sorts the coefficients and their feature names
	print("\t%s\t\t\t\t%s" % (clf.classes_[0], clf.classes_[1])) #Prints the class labels
	print("\n")
	for (coef_1, fn_1), (coef_2, fn_2) in top:
		print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2)) #Prints the feature with its corresponding coefficient
	print("\n")

def classifier(files_google, files_sdl):
	'''This function enables the classification of given labeled lists of text or POS-tag files'''

	y = np.array(["google"]*len(files_google) + ["sdl"]*len(files_sdl)) #Create labels for classification and insert in numpy array
	X = np.array(files_google + files_sdl) #Concatenate all the files into one numpy array
	
	folder = StratifiedKFold(n_splits = 10, shuffle=True) #Create a stratified 10 fold cross validation

	y_total_test = np.array([])
	y_total_observed = np.array([])

	#Pipeline used for vectorization and classification
	pipeline = Pipeline([("vect", TfidfVectorizer(input="filename", binary=True, ngram_range=(1,2))), ("clf", LinearSVC())])

	
	for train_i, test_i in folder.split(X, y):
		pipeline.fit(X[train_i], y[train_i]) #Train the model

		y_obs = pipeline.predict(X[test_i]) #Let the model predict and thus classify
										#	
		y_total_test = np.concatenate([y_total_test, y[test_i]])
		y_total_observed = np.concatenate([y_total_observed, y_obs])

		#print(classification_report(y[test_i], y_obs, target_names=["google","sdl"])) #Classification report per fold

	show_most_informative_features(pipeline.get_params()["vect"], pipeline.get_params()["clf"]) #Show the most informative features
	print(classification_report(y_total_test, y_total_observed, target_names=["google","sdl"]))	#	--> classification report of the total


def main(argv):
	no_of_sentences = argv[1] #Provided (via argv) number of sentences
	test_or_training = argv[2] #Provided test or train set
	text_or_pos = argv[3] #Provided textual or POS-tag data
	if text_or_pos == "text":
		text_or_pos = "txt"
	try:
		#Below the files are gathered from either the training or test data (no. of sentences provided by user), and depending on the user's input, whether textual or POS-tag data is gathered
		if test_or_training == "training" and text_or_pos == "pos":
			google = "{} {} {} {}{}{}".format("google", no_of_sentences, "sent", text_or_pos, "/*.", text_or_pos)
			sdl = "{} {} {} {}{}{}".format("sdl", no_of_sentences, "sent", text_or_pos, "/*.", text_or_pos)
		elif test_or_training == "training" and text_or_pos == "txt":
			google = "{} {} {}{}{}".format("google", no_of_sentences, "sent", "/*.", text_or_pos)
			sdl = "{} {} {}{}{}".format("sdl", no_of_sentences, "sent", "/*.", text_or_pos)
		elif test_or_training == "test" and text_or_pos == "pos":	
			google = "{} {} {} {}{}{}".format("test google", no_of_sentences, "sent", text_or_pos, "/*.", text_or_pos)
			sdl = "{} {} {} {}{}{}".format("test sdl", no_of_sentences, "sent", text_or_pos, "/*.", text_or_pos)
		elif test_or_training == "test" and text_or_pos == "txt":	
			google = "{} {} {}{}{}".format("test google", no_of_sentences, "sent", "/*.", text_or_pos)
			sdl = "{} {} {}{}{}".format("test sdl", no_of_sentences, "sent", "/*.", text_or_pos)

		files_google = glob.glob(google) #Look through the google directory described above and put files in a list
		files_sdl = glob.glob(sdl) #Look through the sdl directory described above and put files in a list

		classifier(files_google, files_sdl) #Start the classification program

	except NameError: #When a wrong word is presented as argv show the following error
		print("{} \n {}".format("Please input the right paramaters", "Example: python3 pipe.py 20 training text"))


if __name__ == "__main__":
	main(sys.argv)