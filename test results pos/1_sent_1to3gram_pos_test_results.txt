	google				sdl


	-2.2347	in rb jjr      		1.9545	dt nnp pos     
	-2.2197	cd cd nnp      		1.8742	pos nn         
	-2.1397	prp prp rb     		1.8117	vbd nn sym     
	-2.0670	vbz in cd      		1.6233	rp nns         
	-1.9337	sym nn vbz     		1.6090	nns cd nnp     
	-1.9276	vbd prp md     		1.5932	pos nn sym     
	-1.8978	fw dt nn       		1.5597	vbn rb rbr     
	-1.8446	vbz cd vbd     		1.5415	dt nn vb       
	-1.8219	jj cd cd       		1.5263	rb vbn prp     
	-1.7617	nn cd vbg      		1.4902	jj vbg prp     


             precision    recall  f1-score   support

     google       0.55      0.50      0.52     72150
        sdl       0.55      0.59      0.57     74282

avg / total       0.55      0.55      0.55    146432

