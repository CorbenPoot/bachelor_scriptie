Hoeveel?
vroeg Varley, met zijn mond bijna tegen Bill's oor.
Bill schudde twijfelachtig zijn hoofd.
"Bijna een dozijn," zei hij.
Varley knikte; het bevestigde zijn eigen schatting.
Toen zaten beide mannen onbeweeglijk, hun oren inspannend voor het geluid van de komst trainer.
Nu bewoog Varley zich een beetje en strekte zijn hand uit in de duisternis raakte Bill de arm en een paar tellen later de laatste hoorde de ritmische slag van de voeten van de paarden, en toch een iets later het doffe rollen van de wielen.
Er was een moment of twee van spanning, onderbroken door de musical tonen van de wachthoorn, en toen uit de duisternis groeide twee lichtvlekjes van de lampen; de ritmische beat trof scherper, de rol van de wielen verdiept en plotseling de koets doemde de nacht op en de leiders ratelden door naar de brug.
Dit was blijkbaar het signaal voor de Dog's Ear-aanval; voor als het metaal van de paardenschoenen klonk op het hout, er was een rende van de Gulch eronder en een groep ruiters omringde de coach, terwijl een man, gemonteerd op een passend zwart paard, reed naast de koetsier en bedekte hem.
"Steek je handen omhoog, Johnson, en ga naar beneden!"
zei hij kortaf.
"Kom nu naar beneden, als een goede jongen, en alarmeer de passagiers niet."
De bestuurder gluurde de duisternis in en vloekte, stemmen vanaf de top van de coach riep vragend en opgewonden, dan een diepte stilte volgde.
"Overtuig hem om snel naar beneden te komen, heren," zei de leider van de bende.
"We willen geen vuurwerk, maar - we bedoelen zaken.
Het is onze show, zie je, en het heeft geen zin om gedoe te maken. '
Twee of drie mannen krabbelden naar beneden en werden meteen omringd, maar de koetsier bewoog zich geen moment; toen wendde hij zich tot iemand op de stoel achter hem en zei iets.
"Kom je of niet?"
vroeg de leider, ongeduldig, en hij imiteerde significant de klik van een trekker met zijn lippen.
Johnson keek naar beneden.
