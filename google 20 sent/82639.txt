"U stelt voor," antwoordde ik, "om uit de woningen van te vliegen man, om te wonen in die wildernis waar de dieren van het veld je zullen zijn alleen metgezellen.
Hoe kun je, die verlangen naar de liefde en sympathie van de mens, volharden in deze ballingschap?
Je zult terugkeren en opnieuw hun vriendelijkheid zoeken, en je zult hun verachting ontmoeten; je kwade passies zullen worden vernieuwd, en je zult dan een metgezel hebben om je te helpen bij de taak van vernietiging.
Dit is misschien niet zo; houd op het argument te betwisten, want ik kan niet instemmen. "
"Hoe onzeker zijn jouw gevoelens!
Maar een moment geleden was je ontroerd door mijn voorstellingen, en waarom verwoede je jezelf opnieuw tegen mijn klachten?
Ik zweer het je, door de aarde die ik bewoon, en door jou die mij gemaakt heeft, dat met de metgezel die je schenkt, zal ik stoppen met de buurt van de mens en wonen, zoals het mogelijk is, op de meest woeste plaatsen.
Mijn kwade passies zal gevlucht zijn, want ik zal sympathie ontvangen!
Mijn leven zal rustig stromen weg, en op mijn stervende momenten zal ik mijn maker niet vervloeken. "
Zijn woorden hadden een vreemd effect op mij.
Ik heb medelijden met hem en soms voelde een wens om hem te troosten, maar wanneer ik naar hem keek, wanneer Ik zag de smerige massa die bewoog en praatte, mijn hart misselijk werd en het mijne gevoelens waren veranderd in die van horror en haat.
Ik probeerde te onderdrukken deze sensaties; Ik dacht dat omdat ik niet met hem sympathiseerde, ik had niet het recht om het kleine deel van geluk dat hem was onthouden was nog in mijn macht om te schenken.
"Je zweert," zei ik, "om onschuldig te zijn; maar niet waar al een mate van boosaardigheid getoond die me redelijkerwijs wantrouwen zou moeten maken U Moge dit niet eens een schijnbeweging zijn die je triomf zal vergroten meer ruimte voor uw wraak? "
Hoe is dit?
Ik moet niet worden bespot en ik eis een antwoord.
als Ik heb geen banden en geen genegenheid, haat en ondeugd moeten mijn deel zijn; de liefde van een ander zal de oorzaak van mijn misdaden vernietigen, en ik zal het doen een zaak worden waarvan het bestaan ​​iedereen onwetend zal zijn.
Mijn ondeugden zijn de kinderen van een gedwongen eenzaamheid die ik verafschuw, en mijn deugden zullen noodzakelijkerwijs ontstaan ​​wanneer ik in gemeenschap leef met een gelijke.
Ik zal voelen de aandoeningen van een gevoelig wezen en verbonden raken met de keten van bestaan ​​en gebeurtenissen waarvan ik nu ben uitgesloten. "
Ik pauzeerde even om na te denken over alles wat hij had verteld en de verschillende argumenten die hij had gebruikt.
Ik dacht aan de belofte van deugden die hij had getoond bij de opening van zijn bestaan ​​en de daaropvolgende ziekte van alle vriendelijke gevoelens door de walging en minachting die zijn beschermers hadden gemanifesteerd naar hem toe.
