"Dus ik zal mijn hoofd knikken," dacht meneer Cruncher verbaasd, "naar alle evenementen zal ze dat zien. "
En dat deed ze.
"Is er nu lawaai in de straten?"
vroeg Miss Pross opnieuw, op dit moment.
Weer knikte meneer Cruncher met zijn hoofd.
"Ik hoor het niet."
"Ben je binnen een uur doof geworden?"
zei meneer Cruncher, herkauwend, met zijn geest veel gestoord; "Wot is naar haar gekomen?"
"Ik voel," zei Miss Pross, "alsof er een flits en een neerstorting was geweest, en die crash was het laatste wat ik ooit in dit leven zou horen. '
"Blest als ze niet in een rare staat is!"
zei Mr. Cruncher, meer en meer gestoord.
"Wat kan zij zijn geweest, om haar moed te behouden?
Hark!
Er is de rol van die vreselijke karren!
U kunt dat horen, mevrouw? "
"Ik hoor het," zei Miss Pross, ziende dat hij tot haar sprak, "niets.
O mijn goede man, er was eerst een grote botsing, en toen een grote stilte, en die stilte lijkt vast en onveranderlijk te zijn, nooit te zijn gebroken meer zolang mijn leven duurt. "
"Als ze de rol van die vreselijke karren niet hoort, nu bijna bij hen het einde van de reis, "zei meneer Cruncher, met een blik over zijn schouder," hij is van mij mening dat ze inderdaad nooit iets anders in deze wereld zal horen. "
En inderdaad heeft ze dat nooit gedaan.
XV.
