"Hoe kan je zo lachen, Lizzy?” Hun aanhankelijke moeder deelde al hun verdriet; ze herinnerde zich wat ze had zelf bij een soortgelijke gelegenheid, vijf-en-twintig jaar, geleden geleden.
"Ik ben er zeker van," zei zij, "ik huilde twee dagen lang samen, Kolonel Het regiment van Miller ging weg.
Ik dacht dat ik mijn hart had gebroken. '
"Ik weet zeker dat ik _mine_ zal breken," zei Lydia.
"Als iemand maar naar Brighton kon gaan!"
merkte mevrouw Bennet op.
"Oh ja - als je maar naar Brighton kunt gaan!
Maar papa is het onaangenaam."
"Een beetje zeebaden zou me voor eeuwig in leven houden."
"En mijn tante Phillips weet zeker dat het _me_ veel goeds zal doen," heb Kitty toegevoegd.
Dat was het soort jammerklachten dat onophoudelijk doorklonk Longbourn House.
Elizabeth probeerde door hen te worden afgeleid; maar alle zintuigen van plezier was in schaamte verloren.
Ze voelde opnieuw de gerechtigheid van de heer Darcy bezwaren; en nooit was ze zo geneigd geweest om de zijne te vergeven inmenging in de opvattingen van zijn vriend.
Maar de somberheid van Lydia's vooruitzicht werd spoedig weggevaagd; voor haar ontving een uitnodiging van mevrouw Forster, de vrouw van de kolonel van het regiment, om haar naar Brighton te begeleiden.
Deze onschatbare vriend was een heel jonge vrouw, en heel recent getrouwd.
Een gelijkenis met een goed humeur en een goed humeur had haar en Lydia aan elkaar en uit haar aanbevolen hun kennis van drie maanden was intiem geweest.
De vervoering van Lydia bij deze gelegenheid, haar aanbidding van mevrouw Forster, de vreugde van mevrouw Bennet en de vernedering van Kitty zijn nauwelijks te beschrijven.
Geheel onattent voor de gevoelens van haar zus, Lydia vloog door het huis in rusteloze extase, oproepend voor iedereen gefeliciteerd, en lachen en praten met meer geweld dan ooit; terwijl de ongelukkige Kitty in de salon doorgaf met een rep van haar lot in termen zo onredelijk als haar accent was peevish.
'Ik begrijp niet waarom mevrouw Forster het niet net zo goed moet vragen als Lydia,' zei ze: "Hoewel ik haar geen vriend ben.
Ik heb net zo veel recht om te worden gevraagd zoals zij heeft, en meer nog, want ik ben twee jaar ouder. "
