"Ik vrees, meneer Darcy," merkte juffrouw Bingley half fluisterend op "dat dit avontuur heeft nogal je bewondering voor haar mooie ogen beïnvloed. "
"Helemaal niet," antwoordde hij; "Ze werden opgefleurd door de oefening."
A korte pauze volgde deze toespraak, en mevrouw Hurst begon opnieuw: "Ik heb een overdreven achting voor Miss Jane Bennet, zij is echt een zeer lieve meid, en ik wou dat ze met heel mijn hart goed gesetteld was.
Maar met zo'n vader en moeder, en zulke lage connecties, ik ben bang dat er is geen kans. "
"Ik denk dat ik je heb horen zeggen dat hun oom een ​​advocaat is Meryton.” "Ja; en ze hebben een andere, die ergens in de buurt van Cheapside woont. '
"Dat is het kapitaal", voegde haar zus eraan toe en ze lachten allebei hartelijk.
"Als ze ooms genoeg hadden om _all_ Cheapside" te vullen, "riep Bingley," het zou ze niet een stuk minder aangenaam maken. "
"Maar het moet hun kansen op het trouwen met mannen van wie dan ook aanzienlijk verminderen aandacht in de wereld, "antwoordde Darcy.
Aan deze toespraak gaf Bingley geen antwoord; maar zijn zussen gaven het hun hartelijke instemming, en hebben hun vrolijkheid voor enige tijd ten koste van de vulgaire relaties van hun geliefde vriend.
Met een hernieuwing van tederheid keerde ze echter terug naar haar kamer de eetzaal verlaten en met haar gaan zitten tot ze bij de koffie werd gedeponeerd.
Ze was nog steeds erg arm, en Elizabeth zou haar helemaal niet stoppen laat in de avond, toen ze het comfort had haar te zien slapen, en wanneer het haar nogal juist leek dan prettig dat ze moest gaan beneden zelf.
Bij het betreden van de salon vond ze het geheel feest bij Loo, en werd onmiddellijk uitgenodigd om zich bij hen aan te sluiten; maar vermoedend om hoog te spelen, weigerde ze het en maakte haar zus tot de excuus, zei dat ze zichzelf zou amuseren voor de korte tijd dat ze kon blijven hieronder, met een boek.
De heer Hurst keek haar verbaasd aan.
"Leest u liever naar kaarten?"
zei hij; "Dat is nogal uniek."
"Miss Eliza Bennet," zei Miss Bingley, "veracht kaarten.
Ze is geweldig lezer, en heeft geen plezier in iets anders. "
"Ik verdien zulk een lof en dergelijke censuur niet," riep Elizabeth; "Ik ben _not_ een geweldige lezer, en ik heb plezier in veel dingen. "
"Bij het verzorgen van je zuster weet je zeker dat je plezier hebt," zei Bingley; "en Ik hoop dat het snel zal toenemen door haar redelijk goed te zien. '
Elizabeth bedankte hem uit haar hart en liep toen naar de tafel waar een paar boeken leken.
