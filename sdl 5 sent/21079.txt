_Dr.
Seward's Diary._ _28 oktober._--Wanneer het telegram kwam de aankondiging van de aankomst in Galatz denk ik niet dat het zo'n schok voor ieder van ons zoals wij hadden verwacht.
Het is waar dat we niet weten waar of hoe, of wanneer de bout zou komen; maar ik denk dat we allemaal hadden verwacht dat iets vreemds zou gebeuren.
De vertraging van aankomst op Varna maakte ons individueel tevreden dat dingen niet precies zoals we hadden verwacht, we wachtten maar te leren waar de verandering zou optreden.
Niettemin was echter een verrassing.
