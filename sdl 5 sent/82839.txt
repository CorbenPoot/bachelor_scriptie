Dit handschrift zal ongetwijfeld hebben het grootste plezier; maar voor mij, die hem kennen, en die horen uit zijn eigen mond met wat interesse en sympathie, lees ik dit in de toekomst!
Zelfs nu, als ik begin mijn taak zijn full-gestemde stem zwelt in mijn oren; zijn glanzende ogen staren op mij met al hun melancholie zoetheid, ik zie zijn dunne kant opgeworpen in animatie, terwijl de lineaments van zijn gezicht zijn bestraald door de ziel binnen.
Vreemd en angstaanjagend moet zijn verhaal, vreselijke storm die omarmde de gallant vaartuig op zijn beloop en spoelde het-dus!
Hoofdstuk 1 Ik ben door geboorte een Genevese en mijn familie is één van de meest onderscheiden van deze republiek.
Mijn voorouders waren jarenlang counselors en staalmeesters, en mijn vader had een aantal publieke situaties met eer en reputatie.
