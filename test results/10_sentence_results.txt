        google                          sdl


        -4.0772 daar                    4.8534  aljosja
        -4.0478 alyosha                 4.6548  vervolgens
        -4.0190 de zijne                4.6338  uw
        -3.9847 jij                     3.9182  wij
        -3.6451 zijne                   2.9313  één
        -3.4520 geweest                 2.7891  zeer
        -3.0852 je                      2.6863  zij
        -3.0268 binnen                  2.4654  één van
        -2.8792 toe                     2.3499  it
        -2.4588 af                      2.3235  mij
        -2.4179 zich                    2.2882  gezegd
        -2.2987 jouw                    2.2448  sommige
        -2.2250 waar                    2.2266  collega
        -2.2122 het niet                2.1844  indien
        -2.1495 een van                 2.0960  tegelijk
        -2.1445 ik het                  1.9580  sir
        -2.1319 om te                   1.7944  mijnheer bloom
        -2.1234 dat niet                1.7906  don
        -2.1184 te zijn                 1.7697  via
        -2.0823 meneer                  1.7624  alle


             precision    recall  f1-score   support

     google       0.91      0.90      0.90      7215
        sdl       0.90      0.91      0.91      7429

avg / total       0.91      0.91      0.91     14644