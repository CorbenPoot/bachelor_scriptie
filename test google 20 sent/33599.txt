"-_ Pall Mall Gazette... "Als een geestige dialoog en een gevoel van sfeer een goede komedie kunnen zijn, hier is een van de beste.
"-_ T. P.'s Weekly... Een verbolgen samenleving DOOR A. BROWNLOW FFORDE ENKELE MENINGEN OVER MR. VOORDELEN VAN FFORDE: 'Misschien is het wel door de heer Kipling geschreven in een van zijn lichtere gemoedstoestanden.
Overvloed aan humor en extreem racy.
"-_ Daily Chronicle... "Is een levendige schrijver, en bezit een aanzienlijk fonds van humor.
"-_ Manchester Examiner... Het duel DOOR ALEXANDER KUPRIN "Men aarzelt niet om 'The Duel' aan iemand op de kijk uit voor echt goede fictie.
"-_ Globe... "Kuprin schrijft met de levendigheid en de autoriteit die komt uit de eerste hand ervaring.
Het moet worden gelezen om het te waarderen kracht.
"-_ Aberdeen Journal... LONDEN: GEORGE ALLEN & UNWIN LIMITED LONDEN: ST. HET THEATER VAN JAMES Lessee en Manager: Mr. George Alexander 14 februari 1895 * * * * * John Worthing, JP: Mr. George Alexander.
Algernon Moncrieff: Mr. Allen Aynesworth.
Canon Chasuble, DD: Mr. HH Vincent.
Merriman: Mr. Frank Dyall.
Lane: Mr. F. Kinsey Peile.
Lady Bracknell: Miss Rose Leclercq.
Hon.
Gwendolen Fairfax: Miss Irene Vanbrugh.
Cecily Cardew: Miss Evelyn Millard.
Miss Prism: Mevr.
George Canninge.
EERSTE HANDELING Scènenr.
Ochtendkamer in de flat van Algernon in Half-Moon Street.
