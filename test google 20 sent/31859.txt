Aldus sprekend bereikten we de grote ijzeren poort, versierd met de Beierse leeuwen.
Ik belde.
De poort werd geopend, de baron trok aan de kant om de zijne toe te staan "Boarders" om binnen te komen, en deze op zijn beurt gesignaleerd aan hem om te nemen voorrang.
De commandant majoor, baron Stefan von Stengel, zeer rechtopstaand, hoofd vastgehouden hoog, passeerde de poort.
De bewaker, volledig bewapend, stond op aandacht, opgesteld in twee rijen.
Bij een bestelling van het _Feldwebel_, "_Hurrah für den Major_," riepen twintig rekruten met een enkele stem.
De nacht was gevallen.
Alle ramen van het fort, die onzichtbaar waren geweest zolang we buiten de muren waren, werden nu gezien om te worden verlicht, en het rood van de stenen manifesteerde zich in het sterrenlicht.
We staken de ophaalbrug.
"Nu de sneeuw is gekomen," zei de commandant, wijzend in de sloot, "we zouden daar een goede ijsbaan kunnen maken."
Hij salueerde, en trok zich terug in zijn kazemat.
* * * * * In feite ben ik mijn tijd hier niet helemaal kwijt, omdat ik dat wel ben erin geslaagd om adequaat te classificeren in de sociale hiërarchie zoals die man Baron von Stengel, die noch held noch genie is, die geen ambitie heeft om bovennatuurlijke deugden te tonen, maar die gewoon een man is met plezierig manieren, verfijnd, goed gefokt, vrij van alle stijfheid, gemakkelijk om erin te komen met, een echt geciviliseerd wezen.
Jij, mijn vrienden, hebt me verwend.
Het is aan jou dat ik altijd had onwetend bleef hoe beperkt het geslacht van 'fatsoenlijk volk' is.
Het oorlog heeft in dit opzicht mijn opvattingen veranderd.
Verhard, vereenvoudigd, vrijer met betrekking tot externe omstandigheden, zo aanpasbaar als iemand zou kunnen wens - als de campagne voorbij is, zal ik wat minder vertrouwelijk zijn dan van vroeger naar mijn soort.
Nu proef ik ze in de massa, elbowing ze onophoudelijk 's ochtends,' s middags en 's nachts gedurende de hele dag van vierentwintig uur, luisterend naar hen terwijl ze praten, kletsen, mopperen, ruzie maken en snurken, naar hen kijken terwijl ze zich vermaken, klagen, spelen, eten, afdingen, de persoonlijke stop intrekken, oordelen vellen, dingen op hun gemak nemen; nu ik er niet langer meer over nadenk het prisma van mijn doctrines en mijn mildheid, maar kijk naar hen als dat zijn ze echt, alle landschappen van het maatschappelijk leven zijn verwijderd, allemaal sociaal attributen ontdaan - er zijn bepaalde categorieën van gedachten die ik beter begrijpen dan voorheen.
Ik begrijp het beter, bijvoorbeeld, kluizenaars, misantropen, jansenisten en alle pessimisten, zowel heidense als christelijke, al diegenen die niets in de mens kunnen zien dan het primitieve beest en die die nooit ophoudt te praten over de erfzonde.
Hoe veel waardeer ik nu goede manieren, het vernis van de cultuur, het masker van fatsoen.
Dit zijn maar uiterlijkheden, dingen die geen uitdrukking geven aan de intieme aard van de mens.
