In mijn gedachten stijgt dit alles tegelijkertijd, Pell-mell, als toevallige testament; het is verschrikkelijk!...
O schande!....
Duitsland, Duitsland, Duitsland.... Helaas!
[30] De kleine Française Wie wacht thuis op mij Heeft ogen die gloeien En een hart van lila..... [31] Na herhaalde verzoeken mocht ik me abonneren op de _Frankfurter Zeitung_.
Drie andere gevangenen hebben zich respectievelijk ingeschreven naar de _Münchener Neueste Nachrichten_, de _Berliner Tageblatt_, en de _Kölnische Zeitung_.
[32] Aardappelbrood.
_Afgedrukt in Groot-Brittannië by_ UNWIN BROTHERS, LIMITED, THE GRESHAM PRESS, WOKING AND LONDON Battles en Bivouacs Het notaboek van een Franse soldaat DOOR JACQUES ROUJON VERTAALD DOOR FRED ROTHWELL _Grote kroon 8vo._ _5s.
netto... "Battles and Bivouacs," door Jacques Roujon, is een levendig recital van de eerste zes maanden van oorlogsvoering.
De taal is direct en onaangetast, ziel-roert, en vrij van overdrijving; elke pagina, ja, elke regel, dwingt aandacht en roept de meest sympathieke interesse op.
Het boek is zowel authentiek als terughoudend in toon, maar wat de lezer het meest treft is zijn buitengewone _oprechtheid_.
Er is nog geen oorlogsboek verschenen dat geeft zo sterk een indruk van de werkelijkheid, of die, op zijn eigen zuivere verdiensten, is meer waard om de woede en het tumult van de strijd te overleven.
Mijn ervaringen op drie fronten DOOR ZUS MARTIN-NICHOLSON _Crown 8vo._ _4s.
6d.
netto... Een levendig verslag van de ervaringen van de auteur in België en Rusland en daarna met de Franse en Engelse troepen.
Praktisch pacifisme en zijn tegenstanders: "Is het vrede, Jehu?"
DOOR DR. SEVERIN NORDENTOFT _Crown 8vo._ _4s.
6d.
netto... Naast het doen van duidelijke suggesties met betrekking tot de lijnen waarop het Vredesbeweging zou na de oorlog moeten gaan werken - suggesties zijn dat wel zowel voor de hand liggend als praktisch - het boek bevat een herdruk van een pamflet geschreven door een inwoner uit de bovenklasse van Schleswig, met kritiek op de voetnoten door een Pruisische geleerde van onbesproken meningen, die zeer sensationeel maakt en persoonlijk getuigenis van de verschrikkelijke ontevredenheid en bittere woede die een overwonnen volk voelt zich in zijn vernederende positie van onderwerping - dus zonder enige twijfel het hoofdobstakel van de vredesbeweging bewijzen heeft te maken met deze onnatuurlijke ontkenning voor de overwonnen mensen van de Rechten van de vrede.
Boven de strijd DOOR ROMAIN ROLLAND VERTAALD DOOR CK OGDEN, MA _Crown 8vo, Cloth._ DERDE INDRUK.
_2s.
