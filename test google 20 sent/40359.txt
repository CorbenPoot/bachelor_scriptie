'Aan een man van genie die een echte waardering van de goddelijke Aeschylus en de nobele en zachtaardige Sophocles, hoewel ze het oneens zijn met sommige delen van hun 'theologie' (Rep.), Deze 'kleinere dichters' moeten verachtelijk en onaanvaardbaar zijn geweest.
Er is geen gevoel sterker in de dialogen van Plato dan een gevoel van de achteruitgang en het verval zowel in de literatuur als in de politiek die is gemarkeerd zijn eigen leeftijd.
Evenmin kan van hem worden verwacht dat hij er met plezier naar kijkt licentie van Aristophanes, nu aan het einde van zijn carrière, die was begonnen door satireert Socrates in the Clouds, en in een soortgelijke geest veertig jaar daarna had hij de grondleggers van de ideale gemenebest in zijn Eccleziazusae, of Female Parliament (Laws).
Er waren andere redenen voor het antagonisme van Plato tegen poëzie.
Het beroep van een acteur werd door hem beschouwd als een degradatie van de mens de natuur, want 'een man in zijn leven' kan niet 'veel delen spelen'; de personages die de acteur speelt lijken zijn eigen karakter te vernietigen, en om niets achter te laten dat echt zichzelf kan worden genoemd.
Geen van beide kan man leeft zijn leven en handelt het.
De acteur is de slaaf van zijn kunst, niet de meester van het.
Vanuit deze visie is Plato meer beslist in zijn uitzetting van het dramatische dan van de epische dichters, hoewel hij dat wel moet weten de Griekse tragici boden nobele lessen en voorbeelden van deugd en patriottisme, waar niets in Homer te vergelijken is.
Maar geweldig dramatische of zelfs grote retorische kracht is nauwelijks consistent standvastigheid of kracht van geest, en dramatisch talent is vaak incidenteel geassocieerd met een zwak of losbandig personage.
In het tiende boek introduceert Plato een nieuwe reeks bezwaren.
heteerst hij zegt dat de dichter of schilder een navolger is, en in de derde graad verwijderd van de waarheid.
Zijn creaties worden niet getest door regel en maatregel; ze zijn alleen verschijningen.
In moderne tijden zouden we dat moeten zeggen kunst is niet alleen maar imitatie, maar eerder de uitdrukking van het ideaal in vormen van zintuigen.
Zelfs het bescheiden beeld van Plato, waaruit zijn argument ontleent een kleur, we moeten volhouden dat de kunstenaar dat mag veredelen het bed dat hij schildert door de plooien van het gordijn, of door de gevoel van thuis dat hij introduceert; en er zijn moderne geweest schilders die zo'n ideaal belang hebben verleend aan een smid of een timmermanswinkel.
Het oog of de geest die zo goed voelt als ziet kan geven waardigheid en pathos naar een verwoeste molen, of een stro-gebouwde schuur (Rembrandt), naar de romp van een schip 'naar zijn laatste thuis' (Turner).
nog mee zou dit van toepassing zijn op de grootste kunstwerken, die de zichtbare belichaming van het goddelijke.
Was Plato gevraagd of de Zeus of Athene van Pheidias was alleen de imitatie van een imitatie niet gedwongen te bekennen dat er iets meer in te vinden was ze dan in de vorm van een sterveling; en dat de regel van proportie waar ze aan voldeden was 'hoger dan welke geometrie of rekenkunde dan ook zou kunnen uiten?'(Statesman.)
Nogmaals, Plato maakt bezwaar tegen de imitatieve kunsten die zij uitdrukken eerder emotioneel dan het rationele deel van de menselijke natuur.
Hij doet niet toegeven Aristoteles theorie, die tragedie of andere serieuze imitaties zijn een zuivering van de passies door medelijden en angst; voor hem lijken ze alleen aan de gelegenheid bieden om zich eraan over te geven.
Toch moeten we dat erkennen we kunnen soms ontregelde emoties genezen door ze uit te drukken; en dat ze vaak sterker worden wanneer ze in onze eigen borst worden opgesloten.
