En nu, omdat we terug zijn op het onderwerp van poëzie, laat dit onze verdediging dient om de redelijkheid van ons eerdere oordeel in te tonen wegzenden van onze staat een kunst met de neigingen die we hebben beschreven; want de rede heeft ons beperkt.
Maar dat mag ze ons niet opleggen elke hardheid of gebrek aan beleefdheid, laten we haar vertellen dat er een is oude ruzie tussen filosofie en poëzie; waarvan er veel zijn bewijzen, zoals het gezegde van 'de schreeuwende hond die tegen haar heer brult', of van iemand die 'machtig is in het ijdele gepraat van dwazen' en 'de menigte van wijzen Zeus omzeild, 'en de' subtiele denkers die bedelaar zijn alle'; en er zijn ontelbare andere tekenen van oude vijandschap tussen de cursussen.
Laten we niettemin onze lieve vriend en de zusterkunst van imitatie, dat als zij alleen haar eigendomstitel zal bewijzen in een welgeordende staat zullen we blij zijn haar te ontvangen - dat zijn we erg bewust van haar charmes; maar we mogen het op die manier niet verraden Waarheid Ik durf te zeggen, Glaucon, dat je net zo gecharmeerd bent van haar als ik, vooral wanneer ze in Homer verschijnt?
Ja, inderdaad, ik ben enorm gecharmeerd.
Zal ik dan voorstellen dat ze mag terugkeren uit de ballingschap, maar alleen op deze voorwaarde - dat ze zichzelf lyrisch verdedigt of een andere meter?
Zeker.
En we kunnen verder schenken aan die van haar verdedigers die geliefden zijn van poëzie en toch geen dichters de toestemming om in haar naam proza ​​te spreken: laat ze niet alleen laten zien dat ze prettig is, maar ook nuttig voor staten en voor het menselijk leven, en we zullen luisteren in een vriendelijke geest; voor als dit kan worden bewezen dat we zeker de gainers zullen zijn - ik bedoel, als er een gebruik is in zowel poëzie als verrukking?
Zeker, zei hij, we zullen de gainers zijn.
Als haar verdediging faalt, dan, mijn lieve vriend, zoals andere personen die dat wel zijn verliefd op iets, maar een terughoudendheid op zichzelf wanneer ze denk dat hun verlangens tegengesteld zijn aan hun interesses, zo ook moeten we daarna de manier van minnaars geeft haar op, hoewel niet zonder een worsteling.
Wij ook worden geïnspireerd door die liefde voor poëzie die het onderwijs van adellijke staten is in ons geïmplanteerd en daarom zouden we haar op haar best laten lijken en waarachtig; maar zolang ze haar verdediging niet kan goedmaken, dit argument van ons zal een charme voor ons zijn, wat we nog zullen herhalen onszelf terwijl we naar haar spanningen luisteren; waar we misschien niet in wegvallen de kinderachtige liefde van haar die de velen boeit.
In ieder geval wij zijn zich ervan bewust dat poëzie die is zoals we hebben beschreven, niet zal zijn serieus beschouwd als het bereiken van de waarheid; en hij die naar haar luistert, vrezen voor de veiligheid van de stad die in hem is, zou op hem moeten zijn bewaak haar verleidingen en maak van onze woorden zijn wet.
Ja, zei hij, ik ben het volkomen met je eens.
Ja, zei ik, mijn lieve Glaucon, want groot is de kwestie die op het spel staat, groter dan verschijnt, of een mens goed of slecht moet zijn.
En wat zal iemand worden geprofiteerd als onder invloed van eer of geld of macht, aye, of onder de opwinding van poëzie negeert hij recht en deugd?
Ja, hij zei; Ik ben overtuigd door het argument, omdat ik dat geloof iemand anders zou zijn geweest.
En toch is er geen melding gemaakt van de grootste prijzen en beloningen die op deugd wachten.
Wat, zijn er nog meer?
Als dat er is, moeten ze van een zijn ondenkbare grootheid.
Waarom, zei ik, wat was ooit geweldig in een korte tijd?
De hele periode van drie scoren jaren en tien is zeker maar een klein ding in vergelijking met de eeuwigheid?
