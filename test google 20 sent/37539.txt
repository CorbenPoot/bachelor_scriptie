(*) "Gilden of verenigingen", "in arti o in tribu."
"Arti" waren ambachts- of handelsgilden, cf.
Florio: "Arte ... een geheel onderneming van om het even welke handel in om het even welke stad of collectieve stad. "
gilden van Florence worden het meest bewonderenswaardig beschreven door Mr Edgcumbe Staley in zijn werk over het onderwerp (Methuen, 1906).
Instellingen met een enigszins vergelijkbaar karakter, genaamd "artel", bestaat vandaag in Rusland, cf.
Sir Mackenzie Wallace's "Rusland", red.
1905: "De zonen ... waren altijd tijdens de werkseizoen leden van een artel.
In sommige van de grotere steden zijn er artels van een veel complexere soort - permanente verenigingen, met groot kapitaal, en in de eerste plaats verantwoordelijk voor de handelingen van het individu leden.
"Het woord" artel ", ondanks zijn schijnbare gelijkenis, heeft, verzekert de heer Aylmer Maude mij, geen verband met "ars" of "Arte."
De wortel is die van het werkwoord "rotisya" om te binden zichzelf door een eed; en het is algemeen toegelaten om alleen te zijn een andere vorm van 'rota', wat nu een 'regimental' betekent bedrijf.
"In beide woorden is het onderliggende idee dat van een lichaam van mannen verenigd door een eed.
"Tribu" was mogelijk niet-Joods groepen, verenigd door gemeenschappelijke afkomst, en inclusief individuen verbonden door huwelijk.
Misschien zijn onze woorden "sekten" of "clans" zou het meest geschikt zijn.
HOOFDSTUK XXII - BETREFFENDE DE SECRETARESSEN VAN PRINSES De keuze van dienaren is van weinig belang voor een prins, en zij zijn goed of niet volgens de discriminatie van de prins.
En de eerste mening die men vormt van een prins, en van zijn begrip, is door de mannen te observeren die hij om zich heen heeft; en wanneer ze capabel zijn en trouw, hij kan altijd als wijs worden beschouwd, omdat hij weet hoe om de bekwame mensen te herkennen en hen trouw te houden.
Maar wanneer ze zijn anders kan iemand geen goede mening over hem vormen, voor de primaire fout wat hij maakte was door ze te kiezen.
Er waren niemand die Messer Antonio da Venafro kende als dienaar van Pandolfo Petrucci, prins van Siena, die Pandolfo niet zou overwegen wees een zeer slimme man in het hebben van Venafro als zijn dienaar.
Omdat er zijn drie klassen van intellecten: een die op zichzelf begrijpt; een andere die waarde hecht aan wat anderen begrepen hebben; en een derde die noch begrijpt ze op zichzelf, noch door het tonen van anderen; de eerste is de meest voortreffelijke, de tweede is goed, de derde is nutteloos.
daarom het volgt noodzakelijkerwijs dat, als Pandolfo niet in de eerste rang was, hij was in de tweede, want wanneer iemand oordeel heeft om het goede te weten en slecht als het wordt gezegd en gedaan, hoewel hij zelf misschien niet de initiatief, maar hij kan het goede en het kwade in zijn dienaar herkennen, en degene die hij kan prijzen en de ander correct; zo kan de dienaar dat niet hoop hem te bedriegen en wordt eerlijk gehouden.
Maar om een ​​prins in staat te stellen een mening te vormen over zijn dienaar is er een test die nooit faalt; wanneer je de dienaar meer aan de zijne ziet denken eigen belangen dan die van u, en innerlijk naar binnen zijn eigen voordeel zoeken alles, zo'n man zal nooit een goede dienaar zijn, en jij ook nooit in staat zijn om hem te vertrouwen; omdat hij die de staat van een ander in zijn bezit heeft handen moet nooit aan zichzelf denken, maar altijd aan zijn prins, en let nooit op zaken waarin de prins niet betrokken is.
