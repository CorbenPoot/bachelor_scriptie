Association door gelijkenis treedt op wanneer twee ideeën in sommigen op elkaar lijken bijzonder.
Ze hoeven in het verleden niet samen te zijn opgetreden, en ook niet na elkaar.
Het feit dat ze een gemeenschappelijk element hebben, is voldoende om een ​​idee naar voren te brengen wanneer de ander in gedachten is: dus de tweestap stelde de eenstap voor.
Associatie daarentegen heeft geen uitleg nodig.
Het wordt geïllustreerd wanneer het idee van het hedendaagse dansen de toon aangeeft idee van verre dansen.
Elke poging om _why_ te laten zien hoe de geest zich op deze manier gedraagt, enige uitleg van de manier waarop de verschillende soorten associatie mogelijk worden gemaakt, zou ons in de fysiologische psychologie brengen, zou een studie van inhouden de hersenen en het zenuwstelsel.
Voor onze doeleinden is het voldoende om houd er rekening mee dat dergelijke associaties wel plaatsvinden.
Zonder hen geen idee kan voorkomen.
Zonder hen is gedachte onmogelijk.
De invloed van dit alles op concentratie moet nog duidelijk worden gemaakt.
We moeten niet vergeten dat elk idee meer dan één medewerker heeft; in het feit dat elk idee over het algemeen een cluster van mogelijke partners heeft.
In plaats van het menuet te suggereren, kan de eenstap de vos zijn geweest draven of de driestappen komen voor de jongedame.
Misschien heeft het haar gemaakt denk aan een jonge man met wie ze danste, of de moeite die ze had door het te leren.
Elk van deze suggesties zou op zijn beurt ook hebben mogelijke connecties met een cluster van ideeën.
Wanneer we denken willekeurig - wanneer we dag dromen, zoals in het gegeven voorbeeld, de sterkste associatie, of de eerste om te worden gewekt, is degene waarop we stilstaan.
Maar... wanneer we denken met een doel, kortom, wanneer we redeneren, we verwerpen alle associaties die geen invloed hebben op ons doel, en selecteer alleen degenen die het serveren.
Concentratie betekent niet, zoals in de volksmond wordt verondersteld, het vasthouden van de geest bevestigd op één object of idee of op één plek.
Het bestaat uit het hebben van een probleem of doel altijd voor een.
Het betekent onze gedachten houden op weg naar het gewenste einde.
Concentratie wordt vaak beschouwd als intense of gerichte aandacht.
