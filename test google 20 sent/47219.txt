", de wetenschap van de ethiek is slechts het antwoord op "Wat is goed gedrag?
", en de metafysica kan beperkt worden tot het probleem "Wat is de realiteit?"
Maar... wanneer we met een van deze te maken krijgen, breken we ze instinctief uit elkaar in kleinere en meer concrete problemen, waardoor de behandeling gemakkelijker wordt, net als een algemene poging om de krachten van zijn vijand te splitsen, zodat hij het kan vernietig één sectie per keer.
Vaak, inderdaad, de eigenlijke verdeling van het grotere probleem in kleinere problemen vormt zijn oplossing, voor we komen uiteindelijk tot een probleem dat praktisch zichzelf beantwoordt, en die we herkennen als zijnde opgenomen in, of een bepaalde vorm van, sommige meer algemeen probleem waar we het antwoord al kennen.
Een man stelt voor zichzelf de vraag: "Wat is de juiste sfeer van overheid, Misschien zal hij eerst en vooral een ander verschil overwegen specifieke activiteiten die mogelijk geacht worden binnen te vallen de sfeer van overheidsinmenging.
Hij vraagt ​​zich misschien af bijvoorbeeld: "Moet de overheid de contractvrijheid verstoren?"
Merk op dat hij hier zijn probleem tijdelijk smaller heeft gemaakt, hij heeft gekozen om het uit elkaar te halen om het stuk voor stuk af te handelen.
Maar zelfs toen hij dit kleinere probleem oploste, zou hij het waarschijnlijk wel vinden het was nodig om dit te doorbreken, en hij zou daarom een ​​specifiek standpunt innemen Voorbeeld: Stel dat een man zoveel uur werkt, en dat negen uur ' een dag werken geeft hem het minimumbedrag waarop hij kan leven en zijn familie ondersteunen.
Zou het verstandig zijn om de wettelijke werkdag te beperken?
zo'n man tot acht uur?
Dit probleem beantwoordt zichzelf praktisch, en dus is verdere indeling niet nodig.
Natuurlijk het antwoord hierop bepaalt niet het antwoord op de oorspronkelijke vraag, voor andere delen moet nog steeds worden overwogen.
In feite zal veel van het succes van ons denken afhangen van hoe we verdelen onze grote problemen in bijkomende problemen, en precies wat onze dochteronderneming of ondergeschikte problemen zijn.
Dit zal tot op zekere hoogte afhangen op onze eigen natuurlijke scherpzinnigheid, en tot op zekere hoogte op louter toeval.
Nee er kunnen starre regels worden vastgelegd.
Het enige advies dat kan worden aangeboden is dat wanneer een denker een probleem opbreekt hij dit met het oog moet doen nut en bepaaldheid.
John Stuart Mill, in een essay over Jeremy Bentham, wees erop dat de geheim van diens kracht en originaliteit van denken lag in de zijne methode, die "binnenkort kan worden omschreven als de detailmethode; van het behandelen van gehelen door ze in hun delen te scheiden, abstracties door ze op te lossen in dingen, klassen en algemeenheden door onderscheid te maken ze in de individuen waarvan ze zijn verzonnen; en elk breken vraag in stukken voordat je probeert het op te lossen. "
De methode was dat niet absoluut origineel met Bentham, maar "welke originaliteit er ook was in de methode, in de onderwerpen waarop hij het toepaste, en in de rigiditeit waarmee hij zich eraan hield, daar was de grootste. '
De systematische denker is voorzichtig met de manier waarop hij marshals zijn moeilijkheden.
Hij weet dat bepaalde problemen behoren te zijn overwogen voor bepaalde anderen, en hij redt zichzelf arbeid en soms een fout door ze in die volgorde te beschouwen.
