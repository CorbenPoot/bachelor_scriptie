Hoe benijdenswaardig Ik ben degene die ik gebied! "
Ik keek hierop neer vanuit het derde verhaal van de redactie van _Le Journal d'Alsace-Lorraine_.
Plotseling begon ik het te begrijpen feodale geest, de cascade van absolute autoriteit en onderwerping die voorheen afgedaald van de soeverein naar de horige via de hiërarchie van baronnen.
Toen ik de Rijn was overgestoken, in de straten van de Duitse steden de de kracht van deze indruk groeide, totdat het positief spookachtig werd.
Overal zag ik een blinde aanbidding van de uniforme, overweldigende vreugde in het dragen; overal de roes van het bevel, alleen geëvenaard door de verrukking van gehoorzaamheid; overal volkomen onwetendheid van het essentiële gelijkheid van mannen, eerst aangetoond in het leven van Christus, en die, eenmaal goed begrepen, de beleefdheid zuivert van slaafsheid, transformeert gehoorzaamheid in aanhankelijke samenwerking, en transfigureert kracht in gebruik; overal, zowel in het leger als in burgerlijk leven, ik zag heren en dienaren, ik zag dezelfde man tegelijk heer en dienaar, heer van degenen onder hem, dienaar van degenen die boven hem staan ​​- maar nergens zag ik burgers.
Ik zag dienaren onderdanig, voorbereid op alles, gehoorzaam aan elk teken, gemechaniseerd en zich daar verheugend, overtuigd dat het was in hun belang om zo te zijn, trots op de vorm en kracht van de ijzeren hand waarvan individueel elke man een van de ontelbare was vingerkootjes.
Ik was geneigd om hierin de dominante eigenschap te zien de Duitse natie.
Een machtige natie, maar een die vervreemd is van de moderne geest: een middeleeuws eilandje midden in het liberale Europa; een geduchte twijfel natie waarin het absolutisme, elders in '89 bezweert, geduldig was het voorbereiden van zijn wraak, en vanwaar op een dag, misschien binnenkort, zou de initiatief van een gevecht tot de dood tussen feodalisme en democratie.
Enkele weken na de scène op de Place de Broglie, M. von Arnim, gehecht aan de Pruisische generale staf, vergezeld me door de kazerne van Potsdam en het kamp van Döberitz.
De regimenten van de bewaker waren bij de oefening.
De volgorde, de stilte, waren absoluut, zelfs in het geval van zij die op hun gemak zijn.
De grond van de boor was niets anders dan een enorme eenzaamheid, zoals die grote elektrische stroomwerken, die verlaten lijken, en waar het enige teken van leven is het zachte gebrom van de dynamo's.
Er leek niets menselijks in deze grond.
Van tijd tot tijd was er een rauw huilen, en de sombere manipulaties geavanceerd, gepensioneerd, naar rechts gereden of naar links.
"Wat een fijn leger automaten!"
Zei ik zachtjes.
"Dat is het," riep de heer von Arnim uit, greep naar de opmerking die dat had alleen voor mijn eigen stichting, als eulogium.
"In Frankrijk, jij individueel initiatief cultiveren, maar we vermijden het als de plaag.
De hele doel van onze training is om het op te splitsen.
Alles wat we nodig hebben is produceren somnambulists, die dergelijke en dergelijke actie op zulk en dat uitvoeren bestellen; niet reflecterend, niet reagerend, maar louter passief gedragend door instinct, reageert op de bestelling als een goed opgeleide volbloed reageert tot de druk van je knie.
