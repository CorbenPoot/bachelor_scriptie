Als je gelijk hebt in jouw mening, en rechtvaardigheid is wijsheid, dan alleen met rechtvaardigheid; maar als ik gelijk heb, dan zonder gerechtigheid.
Ik ben heel blij, Thrasymachus, om je niet alleen te zien instemmend te zijn en afwijkende meningen, maar antwoorden die uitstekend zijn.
Dat is uit beleefdheid aan jou, antwoordde hij.
Je bent heel aardig, zei ik; en zou je de goedheid ook voor hebben laat me weten, of je denkt dat een staat, of een leger, of een groep van overvallers en dieven, of een andere bende van kwaaddoeners zou überhaupt kunnen handelen als hebben ze elkaar verwond?
Nee, zei hij, dat konden ze niet.
Maar als ze zich onthouden van het verwonden van elkaar, dan kunnen ze handelen samen beter?
Ja En dit komt omdat onrecht verdeeldheid en haat veroorzaakt en vechten en rechtvaardigheid schenkt harmonie en vriendschap; is dat niet waar, Thrasymachus?
Ik ben het ermee eens, zei hij, omdat ik niet met je ruzie wil maken.
Hoe goed van je, zei ik; maar ik zou ook graag willen weten of onrecht, met de neiging om haat op te wekken, waar die ook is, onder slaven of onder vrije mannen, maakt hen niet elkaar en stel ze in afwijking en maak ze niet in staat tot gemeenschappelijke actie?
Zeker.
En zelfs als onrecht alleen in twee te vinden is, zullen ze dan geen ruzie maken en vechten en vijanden worden van elkaar en de rechtvaardigen?
Ze zullen.
En stel dat er onrechtvaardigheid is bij één persoon, zou je wijsheid zeggen dat ze verliest of dat ze haar natuurlijke kracht behoudt?
Laten we aannemen dat ze haar kracht behoudt.
Toch is niet de macht die onrechtvaardigheid van een dergelijke aard uitoefent overal waar ze haar verblijfplaats neemt, of het nu in een stad is, in een leger, in een familie, of in een ander lichaam, dat lichaam wordt om te beginnen weergegeven niet in staat tot verenigde actie vanwege opruiing en afleiding; en wordt het niet zijn eigen vijand en in strijd met alles dat tegenwerkt het en met de rechtvaardigen?
Is dit niet het geval?
Ja zeker.
En is onrecht niet even dodelijk als het bestaat in één persoon; in de eerste plaats waardoor hij niet in staat is tot actie omdat hij dat niet is in eenheid met zichzelf, en op de tweede plaats waardoor hij een vijand van wordt zichzelf en de rechtvaardige?
Is dat niet waar, Thrasymachus?
Ja En o mijn vriend, ik zei, de goden zijn zeker rechtvaardig?
