Gewoon omdat iemand anders is tevreden met een bepaalde oplossing, dat is nee reden waarom je zou moeten zijn.
Je moet direct omgaan met de feiten, gegevens en fenomenen in overweging; niet met de meningen van anderen over die feiten, gegevens en verschijnselen.
Je moet jezelf niet afvragen of de pragmatici gelijk hebben, of dat de nominalisten dat wel zijn rechts, of de socialisten, of de evolutionisten, of de democraten, of de Presbyterianen, of de hedonisten, of wat niet.
Je zou het niet moeten vragen jezelf die 'school' van denken waar je bij zou moeten horen.
Je zou moeten denk een probleem uit _voor jezelf_, op elke manier die uitdrukking impliceert.
Aan het einde kom je er overigens misschien in grote lijnen mee akkoord met een of andere stroming.
Dit is echter alleen per ongeluk, en jouw gedachte zal waarschijnlijk eerder waar zijn.
Maar dat zou u nooit moeten doen eens zijn met een school van denken evenmin als onafhankelijk denken leidt jij ook.
Van de problemen die op deze manier worden behandeld, zullen sommigen tien minuten duren, anderen een week.
Als je een bijzonder hardnekkig probleem tegenkomt het kan het beste zijn om het een tijdje te laten staan, bijvoorbeeld een week of twee of zelfs langer, en ga door met andere problemen.
Wanneer problemen dus steeds terugkeren het kan maanden, zelfs jaren duren voordat een bevredigende oplossing is gevonden is bereikt.
In dergelijke gevallen zou je bereid moeten zijn om maanden te geven en zelfs jaren naar hun oplossing.
Als een probleem niet belangrijk genoeg is om zo veel tijd aan u besteden, kan worden gedwongen om het te verlaten; maar dat zou je wel moeten doen houd constant in het achterhoofd dat je het nog niet hebt opgelost, en jij moet bereid zijn om anderen toe te geven dat je het niet hebt opgelost.
Nooit sta louter intellectuele luiheid toe om je twijfels te onderdrukken en je te maken denken dat je een probleem hebt opgelost, wanneer je het in je hart weet dat je jezelf in de staat van geloof hebt gewerkt om alleen maar te redden je geestelijk ongemak.
Wanneer de meeste van uw problemen zijn opgelost en uw opvattingen zijn gemaakt definitief kunt u uw lezing hervatten.
U kunt doorgaan naar andere boeken op het onderwerp.
Wat betreft de suggestie dat een ander boek over het onderwerp zou kunnen worden behandeld op dezelfde manier als deze eerste: dit zal grotendeels afhangen van het individuele onderwerp.
Het zal afhangen van wat boeken zijn geweest geschreven over dat onderwerp.
Als er geen volledig of adequaat de veld, of als er twee of meer goede boeken zijn die radicaal representeren verschillende gezichtspunten, zou meer dan één boek waarschijnlijk moeten worden bestudeerd op deze veelomvattende manier.
Maar dit moet aan de lezer worden overgelaten discretie.
