Net als bij mode in kleding zijn er mensen die streven om anderen te imiteren, dus er zijn mensen die zich volledig wijden om 'anders' te zijn.
Hun grootste angst is dat ze zullen worden genomen voor "een van de maffia".
Ze kleden zich zo uniek mogelijk aan om "individualiteit" te verwerven.
We hebben dezelfde mensen in het rijk van het denken.
Ze zijn constant in paniek om te voorkomen dat ze zeggen iets dat iedereen zegt.
Ze zeggen dingen niet omwille van hen van de waarheid, maar voor humor of paradox.
Hun grote vreugde is om te bevestigen of verdedig iets "nieuw" ongeacht zijn waarheid; iets heerlijks radicaal die iedereen zal choqueren en zichzelf zal verschrikken.
Het slechtste deel hiervan is dat deze mensen er geleidelijk aan toe komen hun stellingen als waar, net zoals een leugenaar hem eindelijk gaat geloven eigen leugens.
De enige remedie voor zo'n mentale conditie is een constante oprechtheid in elke mening die we opdoen.
Mensen worden vaak in de fout geleid door een motief dat op zichzelf niet te raden is - het verlangen naar originaliteit.
Maar... ze kiezen het verkeerde pad naar hun doel.
Als je originaliteit maakt en radicaliteit je doel, je zult waarheid noch originaliteit bereiken.
Maar als je waarheid je doel maakt, zul je zeer waarschijnlijk de waarheid krijgen, en originaliteit komt vanzelf.
Er zijn honderden vooroordelen, honderden vormen van vooroordelen.
Er is bijvoorbeeld het vooroordeel van het conservatisme, dat zich manifesteert zelf in een vage vrees dat als de huidige orde in om het even welk werd veranderd vooral - als vrouwen de stem kregen, als het socialisme zou zegevieren, als er een nieuw archiefsysteem op kantoor zou worden geïnstalleerd, zou dat alles zijn Niet gevonden Maar ik kan niet adequaat omgaan met alle vormen van vooringenomenheid die komt me voor de geest.
Het onderscheidende kenmerk van de grote denkers van de eeuwen was hun relatieve vrijheid van de vooroordelen van hun tijd en gemeenschap.
Om deze vooroordelen te vermijden, moet men constant zijn en compromisloos klinken zijn eigen meningen.
Eeuwige waakzaamheid is het prijs van een open geest.
* * * * * Vooroordelen zijn niet het enige gevaar dat op de loer ligt voor de would-be denker.
In zijn pogingen om zich te ontdoen van vooroordelen is hij aansprakelijk vallen in een nog grotere intellectuele zonde.
