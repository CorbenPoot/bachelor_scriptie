Maar verleent hij daarom geen voordeel wanneer hij voor niets werkt?
Zeker, hij verleent een voordeel.
En nu, Thrasymachus, is er geen twijfel meer dat het geen kunst is noch regeringen voorzien in hun eigen belangen; maar zoals we eerder waren zeggen, ze heersen en zorgen voor de belangen van hun onderdanen wie zijn de zwakkere en niet de sterkere - in hun voordeel wonen ze en niet ten goede van de meerdere.
En dit is de reden, mijn liefste Thrasymachus, waarom, zoals ik zojuist zei, niemand bereid is regeren; omdat niemand graag de hervorming van het kwaad in handen neemt die zonder zorg niet zijn zorg zijn.
Voor, bij de uitvoering van zijn werk, en bij het geven van zijn orders aan een ander, doet de ware kunstenaar dat niet acht zijn eigen belang, maar altijd dat van zijn onderdanen; en daarom opdat heersers mogelijk willen regeren, moeten ze worden betaald in een van drie manieren van betaling, geld of eer, of een sanctie voor weigering.
Wat bedoel je, Socrates?
zei Glaucon.
De eerste twee betalingsmodi zijn begrijpelijk genoeg, maar wat de straf is, begrijp ik niet, of hoe een boete een betaling kan zijn.
U bedoelt dat u de aard van deze betaling niet begrijpt de beste mannen is de grote aanleiding om te regeren?
Natuurlijk weet je dat ambitie en hebzucht worden als een schande beschouwd, zoals ze inderdaad zijn?
Waar.
En om deze reden, zei ik, geld en eer hebben geen aantrekkingskracht op hen; goede mannen willen niet openlijk betaling eisen om te regeren en dus om de naam van huurlingen te krijgen, noch door in het geheim zichzelf te helpen uit de openbare inkomsten om de naam van dieven te krijgen.
En niet zijn ambitieus, ze geven niets om eer.
Daarom moet de noodzaak zijn op hen gelegd, en ze moeten ertoe worden aangezet om te dienen vanuit de angst voor straf.
En dit is, zoals ik veronderstel, de reden waarom de forwardness in functie treden, in plaats van te wachten om gedwongen te worden, werd geacht oneervol.
Nu is het ergste van de straf dat hij die weigert te regeren, kan worden geregeerd door iemand die erger is dan hijzelf.
En de angst hiervoor, zoals ik denk, wekt het goede om aan het werk te gaan, niet omdat ze dat zouden doen, maar omdat ze niet kunnen helpen - niet onder het idee dat ze zelf enig voordeel of genot zullen hebben, maar als een noodzaak, en omdat ze niet in staat zijn om de taak van regeren te plegen voor iemand die beter is dan zichzelf, of inderdaad als goed.
Voor daar is reden om te denken dat als een stad volledig uit goede mensen zou bestaan, dan zou het vermijden van een functie evenzeer een twistpunt zijn als tot het verkrijgen van een functie is op dit moment; dan moeten we een duidelijk bewijs hebben dat het ware heerser is niet van nature bedoeld om zijn eigen belang te beschouwen, maar dat van zijn onderwerpen; en iedereen die dit wist zou er liever voor kiezen een voordeel ontvangen van iemand anders dan de moeite hebben om te overleggen een Tot zover ben ik het eens met Thrasymachus dat rechtvaardigheid het is belang van de sterkere.
Deze laatste vraag hoeft niet verder te zijn besproken op dit moment; maar wanneer Thrasymachus zegt dat het leven van de onrechtvaardig is voordeliger dan dat van de rechtvaardige, zijn nieuwe verklaring lijkt mij van een veel ernstiger karakter.
Wie van ons heeft echt gesproken?
