- zouden mannen ze niet hebben gedwongen om te blijven waar ze waren, of ze gevolgd te hebben om te krijgen Opleidingen Maar dat deden ze niet; en daarom kunnen we daaruit afleiden dat Homer en alle dichters zijn alleen maar navolgers, die de schijn van doen dingen.
Want als een schilder door een kennis van figuur en kleur kan schilderen schoenmaker zonder enige praktijk in cobbling, zodat de dichter kan afbakenen elke kunst in de kleuren van de taal, en geef harmonie en ritme aan de schoenmaker en ook voor de generaal; en je weet hoe een verhaal, wanneer beroofd van de ornamenten van de meter, is als een gezicht dat de schoonheid van de jeugd en nooit een ander gehad.
Nogmaals, de imitator heeft geen kennis van de werkelijkheid, maar alleen van uiterlijk.
De schilder schildert, en de maker maakt een teugel en teugels, maar begrijpt ook niet het gebruik van hen - de kennis hiervan is beperkt tot de ruiter; en zo van andere dingen.
Dus we hebben drie kunsten: een van gebruik, een andere van uitvinding, een derde van imitatie; en de gebruiker levert de regel aan de twee anderen.
De fluitspeler zal de goede en slechte fluit en de maker kennen zal vertrouwen in hem stellen; maar de navolger zal het noch weten noch hebben geloof - noch wetenschap noch ware mening kan aan hem worden toegeschreven.
Imitatie is dus verstoken van kennis, omdat het slechts een soort van spel is of sport, en de tragische en epische dichters zijn imitators in de hoogste Diploma En laat ons nu informeren, wat is het vermogen in de mens dat antwoordt imitatie.
Sta mij toe om mijn betekenis uit te leggen: voorwerpen worden anders gezien wanneer in het water en wanneer uit het water, wanneer dichtbij en wanneer bij a afstand; en de schilder of jongleur maakt gebruik van deze variatie om ons opdringen.
En de kunst van meten en wegen en berekenen komt binnen om onze verbijsterde geesten te redden van de kracht van uiterlijk; voor, zoals we zeiden, twee tegengestelde meningen van hetzelfde over hetzelfde en tegelijkertijd kunnen beide niet waar zijn.
Maar wie van hen is waar wordt bepaald door de kunst van het berekenen; en dit is gelieerd aan de beter vermogen in de ziel, omdat de kunst van imitatie erger wordt.
En dezelfde houdingen van het oor als van het oog, van poëzie ook als schilderij.
De imitatie is van acties die vrijwillig of onvrijwillig zijn, waarin er een verwachting is van een goed of slecht resultaat en aanwezig ervaring van plezier en pijn.
Maar is een man in harmonie met zichzelf wanneer hij het onderwerp is van deze tegenstrijdige invloeden?
Is er niet?
eerder een tegenstrijdigheid in hem?
Laat me verder vragen, of hij meer is waarschijnlijk om verdriet te beheersen wanneer hij alleen is of wanneer hij in gezelschap is.
'In het laatste geval.
'Gevoel zou ertoe leiden dat hij zich overgeeft aan zijn verdriet, maar rede en wet beheersen hem en bevelen geduld; omdat hij het niet kan weten of zijn verdrukking goed of slecht is, en dat er geen menselijk wezen is elke grote consequentie, terwijl verdriet zeker een belemmering van het goede is Raadgeving Want als we struikelen, moeten we niet, zoals kinderen, een maken verontwaardigde reacties; we moeten de maatregelen nemen die de rede voorschrijft, niet verhogen een klaagzang, maar een remedie vinden.
En het betere deel van ons is er klaar voor volg de rede, terwijl het irrationele beginsel vol van verdriet is en afleiding bij het herinneren van onze problemen.
Helaas, echter, deze laatste levert de belangrijkste materialen van de imitatieve kunsten.
