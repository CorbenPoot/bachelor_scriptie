Je zult me ​​heel materialistisch vinden, vrees ik.
Maar als je leest, doe dat dan denk dat ik buitengewoon goed ben, dat ik zo hard werk als gewoonlijk, en dat mijn eetlust, waarmee je bekend bent, hier tevreden moet zijn met een dagelijkse vergoeding dat in Parijs amper genoeg zou zijn geweest voor een een maaltijd.
Het was Fritz Magen, de _Gefreiter_, de leidende privépersoon van onze Beierse bewaker, die mij gisteravond dit pakket gaf.
Ik had er niet aan gedacht zo'n meevaller.
In dezelfde gemoedstoestand als elke andere gevangene zat ik te wachten zoals de rest in nummer 17 aan de voet van mijn "bed" voor de frisse uitstraling in de kazemat van de mannen om het hoofd te roepen.
Het is half negen.
Plots gaat de deur open.
"Het appèl," blaast Dutrex, gustily barbend, gevolgd door de _Feldwebel_ en de lantaarn-drager.
Dutrex telt ons snel.
"_Zweiundzwanzig_," hij kondigt het _Feldwebel_ aan.
"Tweeëntwintig."
Hij schudt me bij de hand, te zeggen: "_Gute Nacht, mein Freund; schlafe wohl._ "De ronde gaat voorbij.
Maar Magen, de achterhoede, die op het punt staat de deur te sluiten, legt zijn lantaarn neer, produceert een flinke doos en steekt die in mijn handen op een manier dat is bijna timide.
"_Da_", legt hij mij in het Duits uit, "mijn vrouw stuurde me een belemmeren deze ochtend.
"-" Oh, dank je, "antwoord ik.
Maar hij haast zich met de zijne lantaarn om lid te worden van het _Feldwebel_ in nummer 18.
Zeer geraakt door dit onverwachte teken van vriendschap, wend ik me tot Guido.
We vertellen over de inhoud van de doos.
Vijf appels; twee walnoten; een stuk van dikke pannenkoek, ruikend naar de braadpan van de onbekende Frau_ Magen; en een halve bosbessen taart!
Welk geluk!
