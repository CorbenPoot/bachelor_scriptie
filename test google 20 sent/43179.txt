Dan kan ik de moed opbrengen om te zijn zoals je beschrijft?
Waarom, ja, zei ik, u mag, en als u de woorden 'van een burger' toevoegt, je zult niet veel verkeerd hebben; - hierna, als je wilt, zullen we de verder onderzoek, maar op dit moment zijn we niet op zoek naar moed maar rechtvaardigheid; en voor het doel van ons onderzoek hebben we genoeg gezegd.
Je hebt gelijk, antwoordde hij.
Twee deugden moeten nog worden ontdekt in de staat - ten eerste, matigheid en dan is gerechtigheid het einde van onze zoektocht.
Waar.
Kunnen we rechtvaardigheid vinden zonder ons te bekommeren om matigheid?
Ik weet niet hoe dat kan worden bereikt, zei hij, en ik verlang er ook niet naar dat rechtvaardigheid aan het licht moet worden gebracht en matigheid uit het oog moet worden verloren; en daarom wens ik dat je me de gunst doet om te overwegen matigheid eerst.
Zeker, ik antwoordde, ik zou niet moeten worden gerechtvaardigd door je te weigeren Request Overweeg dan, zei hij.
Ja, ik antwoordde; Ik zal; en voor zover ik kan nu de deugd zien van matigheid heeft meer de aard van harmonie en symfonie dan de voorafgaande.
- Hoezo?
vroeg hij.
Matigheid, antwoordde ik, is het ordenen of controleren van bepaalde genoegens en verlangens; dit is merkwaardig genoeg geïmpliceerd in het gezegde van 'een man die zijn eigen meester is'; en andere sporen van dezelfde notie kunnen zijn gevonden in taal.
Ongetwijfeld, zei hij.
Er is iets belachelijks in de uitdrukking 'meester van zichzelf;' voor de meester is ook de dienaar en de knecht de meester; en in totaal deze wijzen van spreken van dezelfde persoon worden aangegeven.
Zeker.
De betekenis is, geloof ik, dat er in de menselijke ziel een betere en ook een slechter principe; en als het beter is, heeft het slechter onder controle, dan wordt van een man gezegd dat hij de baas over zichzelf is; en dit is een term van lof: maar wanneer, ten gevolge van kwaad onderwijs of associatie, hoe beter principe, dat ook het kleinere is, wordt overweldigd door de grotere massa van het ergste - in dit geval krijgt hij de schuld en wordt hij de slaaf van het zelf genoemd en gewetenloos.
Ja, daar zit ook de reden voor.
En nu, zei ik, kijk naar onze nieuw gecreëerde staat, en daar zul je vind een van deze twee voorwaarden gerealiseerd; voor de staat, als jij zal erkennen, mag terecht de meester van zichzelf worden genoemd, als de woorden 'matigheid' en 'zelfbeheersing' drukken echt de heerschappij van de betere uit gedeeltelijk slechter.
Ja, zei hij, ik zie dat wat je zegt waar is.
Laat me verder opmerken dat het veelvoud en complexe genoegens en verlangens en pijn wordt meestal gevonden bij kinderen en vrouwen en bedienden, en in de zogenaamde vrije mannen die van de laagste en meer talrijke klasse zijn.
