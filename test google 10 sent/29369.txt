Wat een voorrecht!
We zijn er.
nooit alleen.
We slapen, we kleden, we eten, we vermaken onszelf, we lopen rond, we jagen voor luizen, houden we ons bezig met de roep van de natuur, we dromen, we zijn vervuld met verontwaardiging, verzachten we, we strelen de dierbare relieken verborgen in onze knapzakken, trekken we onszelf terug - we doen dit allemaal in het openbaar.
Hoe goed begrijp ik de zin van St. Bernard, de uitdrukking van een monnik, _O beata solitudo, sola beatitudo_!
Soms in de ochtend, wanneer wij ontwaken, dit ontwaken verstoken van waardigheid, vol van eden, wanneer hetzelfde stemmen gabble dezelfde platitudes, in dezelfde eeuwige toegang van steriel verveling, maakt me ziek.
Hoe lang gaat het verder, dit leven in een kudde?
Het lijkt mij dat het effluvium van de menigte, van de zweet van menselijk vee, is doorgedrongen in alle tussenruimten van mijn ziel.
Nee, het is nutteloos; de inspanning om mezelf samen te trekken en wat te worden Ik was vóór deze dagen in de gevangenis te veel voor mijn slechte kracht.
I ben rillend van de kou.
