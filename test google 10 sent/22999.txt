Ik kan je nooit leugens vertellen, "verklaarde ze met een vreemd vuur in haar ogen.
Wat Alyosha boven alles trof, was haar ernst.
Er was geen een spoor van humor of grapjes in haar gezicht nu, in vroeger tijden, plezier en gayety heeft haar nooit verlaten, zelfs niet op haar 'meest serieuze' momenten.
"Er zijn momenten dat mensen van misdaad houden," zei Alyosha peinzend.
Ja!
Ja!
Je hebt mijn gedachte uitgesproken; ze houden van misdaad, iedereen houdt van misdaad, ze houden er altijd van, niet op sommige 'momenten'.
Weet je, het is zo hoewel mensen een afspraak hebben gemaakt om erover te liegen en te liegen het sindsdien.
Ze verklaren allemaal dat ze het kwaad haten, maar stiekem allemaal hou ervan."
"En lees je nog steeds nare boeken?"
