Dan moet een slechte ziel noodzakelijkerwijs een slechte heerser en superintendent zijn, en de goede ziel een goede heerser?
Ja, noodzakelijk.
En we hebben toegegeven dat rechtvaardigheid de voortreffelijkheid van de ziel is, en onrecht het gebrek van de ziel?
Dat is toegegeven.
Dan zullen de rechtvaardige ziel en de rechtvaardige man goed leven, en de onrechtvaardige man ziek zal worden?
Dat is wat uw argument bewijst.
En hij die goed leeft, is gezegend en gelukkig, en hij die ziek leeft, de omgekeerde van blij?
Zeker.
Dan is de rechtvaardige gelukkig en de onrechtvaardige ellendig?
Zo zal het zijn.
