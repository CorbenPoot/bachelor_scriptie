Waar begin je hierin mee?
Vroeg Stephen, terwijl hij een nieuw boek opensloeg.
- Niet meer, zei Comyn.
- Ga maar door, Talbot.
- En het verhaal, mijnheer?
- Na, zei Stephen.
Ga door, Talbot.
Een donkere jongen opende een boek en zette het recht onder het borstwerk van zijn tas.
Hij reciteerde schokken van verzen met een vreemde blik op de tekst: -Want niet meer, lieve herders, huil niet meer Voor Lycidas is je verdriet niet dood, Gezonken hoewel hij onder de waterige vloer is ... Het moet dan een beweging zijn, een realiteit van het mogelijke als mogelijk.
De zin van Aristoteles vormde zichzelf in de gabbelde strofen en zweefde uit in de leergierige stilte van de bibliotheek van Saint Genevieve waar hij had gelezen, beschut tegen de zonde van Parijs, nacht bij nacht.
