vroeg hij.
Matigheid, antwoordde ik, is het ordenen of controleren van bepaalde genoegens en verlangens; dit is merkwaardig genoeg geïmpliceerd in het gezegde van 'een man die zijn eigen meester is'; en andere sporen van dezelfde notie kunnen zijn gevonden in taal.
Ongetwijfeld, zei hij.
Er is iets belachelijks in de uitdrukking 'meester van zichzelf;' voor de meester is ook de dienaar en de knecht de meester; en in totaal deze wijzen van spreken van dezelfde persoon worden aangegeven.
Zeker.
De betekenis is, geloof ik, dat er in de menselijke ziel een betere en ook een slechter principe; en als het beter is, heeft het slechter onder controle, dan wordt van een man gezegd dat hij de baas over zichzelf is; en dit is een term van lof: maar wanneer, ten gevolge van kwaad onderwijs of associatie, hoe beter principe, dat ook het kleinere is, wordt overweldigd door de grotere massa van het ergste - in dit geval krijgt hij de schuld en wordt hij de slaaf van het zelf genoemd en gewetenloos.
Ja, daar zit ook de reden voor.
En nu, zei ik, kijk naar onze nieuw gecreëerde staat, en daar zul je vind een van deze twee voorwaarden gerealiseerd; voor de staat, als jij zal erkennen, mag terecht de meester van zichzelf worden genoemd, als de woorden 'matigheid' en 'zelfbeheersing' drukken echt de heerschappij van de betere uit gedeeltelijk slechter.
Ja, zei hij, ik zie dat wat je zegt waar is.
Laat me verder opmerken dat het veelvoud en complexe genoegens en verlangens en pijn wordt meestal gevonden bij kinderen en vrouwen en bedienden, en in de zogenaamde vrije mannen die van de laagste en meer talrijke klasse zijn.
