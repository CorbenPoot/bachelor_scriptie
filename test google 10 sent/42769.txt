Heel waar, zei hij.
Maar bij de rijke man is dit anders; van hem zeggen we niet dat hij heeft een speciaal aangewezen werk dat hij moet uitvoeren, als hij dat zou doen Leef.
Over het algemeen wordt verondersteld dat hij niets te doen heeft.
Dan heb je nog nooit gehoord van het gezegde van Phocylides, dat zo snel als een man heeft levensonderhoud dat hij deugd moet beoefenen?
Neen, zei hij, ik denk dat hij maar beter iets eerder kan beginnen.
Laten we hier geen geschil mee hebben, zei ik; maar vraag het liever onszelf: is de beoefening van deugd verplicht voor de rijke man, of kan hij zonder leven?
En als het voor hem verplicht is, laten we dan raisen een andere vraag, of dit op dieet zijn van aandoeningen, dat is een belemmering voor de toepassing van de geest bij het timmermanswerk en de mechanische kunst, staat het sentiment niet even in de weg Phocylides?
Daarvan antwoordde hij, er kan geen twijfel over bestaan; zo'n overdreven zorg voor de lichaam, wanneer uitgevoerd buiten de regels van gymnastiek, is het meest vijandig aan de beoefening van deugd.
Ja, inderdaad, antwoordde ik, en even onverenigbaar met het management van een huis, een leger of een staatsbureau; en, wat het belangrijkst is van alle, onverenigbaar met elke vorm van studie of gedachte of zelfreflectie - er is een constant vermoeden dat hoofdpijn en duizeligheid moet worden toegeschreven aan de filosofie, en dus alle inoefening of het beproeven van deugd in de hogere zin is absoluut gestopt; voor een man fantaseert altijd dat hij ziek wordt en constant is bezorgdheid over de toestand van zijn lichaam.
Ja, waarschijnlijk genoeg.
