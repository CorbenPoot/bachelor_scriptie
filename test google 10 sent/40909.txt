Hij is niet onwillig om de verstandige wereld toe te geven in zijn schema van waarheid.
Evenmin beweert hij in de Republiek het onvrijwilligheid van ondeugd, die door hem in de Timaeus wordt onderhouden, Sofist en wetten (Protag., Apol., Gorg.).
Noch de zogenaamde Platonische ideeën die uit een vroegere bestaanstoestand zijn hersteld, beïnvloeden zijn theorie van mentale verbetering.
Toch observeren we in hem de overblijfselen van het oude Socratische doctrine, dat ware kennis van binnenuit moet worden ontlokt, en moet worden gezocht in ideeën, niet in bijzonderheden van betekenis.
Onderwijs, als zegt hij, zal een intelligentieprincipe implanteren dat beter is dan tienduizend ogen.
De paradox dat de deugden één zijn en de verwanten het idee dat alle deugd kennis is, wordt niet volledig verzaakt; de eerst wordt gezien in de suprematie die aan de gerechtigheid wordt gegeven boven de rest; de ten tweede in de neiging om de morele deugden in de intellectuele, en om alle goedheid te centreren in de contemplatie van het idee van het goede.
Het de zintuiglijke wereld wordt nog steeds afgeschreven en geïdentificeerd met een mening toegelaten als een schaduw van het ware.
In de Republiek is hij duidelijk onder de indruk van de overtuiging dat ondeugd vooral voortkomt uit onwetendheid en kan worden genezen door educatie; de menigte kan nauwelijks worden geacht verantwoordelijk voor wat ze doen.
Een vage toespeling op de leer van reminiscentie komt voor in het Tiende Boek; maar Plato's opvattingen over onderwijs geen echte verbinding meer hebben met een vorige bestaanstoestand dan ons eigen; hij stelt alleen voor om datgene wat er is uit de geest te halen - Nu al?
Onderwijs wordt door hem vertegenwoordigd, niet als de vulling van een schip, maar als het draaien van het oog van de ziel naar het licht.
