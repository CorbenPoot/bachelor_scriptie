Eindelijk herstelde de Mock Turtle zijn stem en met tranen over zijn wangen rennend, ging hij weer verder: 'Je hebt misschien niet veel onder de zee geleefd ...' ['Ik niet,' zei hij Alice) - 'en misschien ben je zelfs nooit kennis gemaakt met een kreeft -' (Alice begon te zeggen 'ik heb eens geproefd ...' maar keek haastig toe, en zei 'Nee, nooit') '- dus je kunt geen idee hebben wat een heerlijk iets een Lobster Quadrille is! '
'Nee, inderdaad,' zei Alice.
'Wat voor soort dans is het?'
'Waarom,' zei de Griffioen, 'je vormt eerst een rij langs de kust--' 'Twee lijnen!'
riep de Mock Turtle.
'Zeehonden, schildpadden, zalm, enzovoort; dan, als je alle jelly-fish uit de weg hebt geruimd ... ' 'DAT duurt over het algemeen wat tijd,' onderbrak de Gryphon.
'- je gaat twee keer vooruit -' 'Elk met een kreeft als partner!'
riep de Griffioen.
'Natuurlijk,' zei de Nepschildpad: 'twee keer vooruitgaan, op partners instellen -' '- verander kreeften en ga op dezelfde manier met pensioen', ging de Gryphon verder.
'Dan, weet je,' ging de Nepschildpad door, 'gooi je de ...' 'De kreeften!'
