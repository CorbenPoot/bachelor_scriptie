en bij de bliksemflitsen zag ik de zwarte het grootste deel van de omgekeerde hondenkar en het silhouet van het wiel stil langzaam ronddraaien.
Op een ander moment ging het kolossale mechanisme schrijdend door mij, en gingen bergopwaarts richting Pyrford.
Als je dichterbij kwam, was het ding ongelooflijk raar, want het was gewoon niets ongevoelig machine rijden onderweg.
Machine was het, met een rinkelen metallic tempo, en lange, flexibele, glinsterende tentakels (een van die greep een jonge dennenboom) slingerend en ratelend over het vreemde Lichaam:  Het koos zijn weg terwijl het verder schreed en de koperen kant op kap die erop stond, bewoog zich heen en weer met het onvermijdelijke suggestie van een hoofd dat rondkeek.
Achter het hoofdlichaam was een enorme massa van wit metaal zoals een gigantische vissersmand, en trekjes van groene rook spoot uit het gewricht van de ledematen als het monster geveegd door mij.
En in een oogwenk was het weg.
Zoveel zag ik toen, allemaal vaag voor het flikkeren van de bliksem, in verblindende hoogtepunten en dichte zwarte schaduwen.
Terwijl het voorbij kwam, werd een exultant oorverdovend gehuil opgericht dat de donder - "Aloo!
Aloo!
"- en in een andere minuut was het met zijn metgezel, een halve mijl verderop, bukend over iets in het veld.
