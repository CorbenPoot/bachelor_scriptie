"U stelt voor," antwoordde ik, "om uit de woningen van te vliegen man, om te wonen in die wildernis waar de dieren van het veld je zullen zijn alleen metgezellen.
Hoe kun je, die verlangen naar de liefde en sympathie van de mens, volharden in deze ballingschap?
Je zult terugkeren en opnieuw hun vriendelijkheid zoeken, en je zult hun verachting ontmoeten; je kwade passies zullen worden vernieuwd, en je zult dan een metgezel hebben om je te helpen bij de taak van vernietiging.
Dit is misschien niet zo; houd op het argument te betwisten, want ik kan niet instemmen. "
"Hoe onzeker zijn jouw gevoelens!
Maar een moment geleden was je ontroerd door mijn voorstellingen, en waarom verwoede je jezelf opnieuw tegen mijn klachten?
Ik zweer het je, door de aarde die ik bewoon, en door jou die mij gemaakt heeft, dat met de metgezel die je schenkt, zal ik stoppen met de buurt van de mens en wonen, zoals het mogelijk is, op de meest woeste plaatsen.
Mijn kwade passies zal gevlucht zijn, want ik zal sympathie ontvangen!
Mijn leven zal rustig stromen weg, en op mijn stervende momenten zal ik mijn maker niet vervloeken. "
Zijn woorden hadden een vreemd effect op mij.
