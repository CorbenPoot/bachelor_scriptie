Ze hebben daar op gewacht.
Als ze daar faalde, was dat wel zo niets in haar.
Ze zag er charmant uit toen ze naar buiten kwam in het maanlicht.
Dat kon niet ontzegd worden.
Maar de sluwheid van haar acteren was ondraaglijk en groeide slechter als ze verder ging.
Haar gebaren werden absurd kunstmatig.
Ze overdreven alles dat ze te zeggen had.
De prachtige passage-- Je weet dat het masker van de nacht op mijn gezicht ligt, Anders zou een meisje blozen met mijn wang Want dat wat je me hebt horen spreken vanavond - werd vereerd met de pijnlijke precisie van een schoolmeisje dat is geweest geleerd om te reciteren door een tweederangs hoogleraar in de dictie.
Wanneer zij leunde over het balkon en kwam tot die prachtige lijnen-- Hoewel ik vreugde in je heb, Ik heb geen vreugde van dit contract vanavond: Het is te onbezonnen, te weinig geadviseerd, te plotseling; Te graag de bliksem, die ophoudt te zijn Eerder kan men zeggen: "Het wordt lichter."
Liefje, goedenavond!
