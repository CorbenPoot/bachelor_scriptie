Ik leek het te willen houden tot het punt van zijn waanzin - iets dat ik met de patiënten vermijd zoals ik de mond van de hel zou doen.
(_Mem._, onder welke omstandigheden zou ik de hel van de hel niet vermijden?)
_Omnia Romæ venalia sunt._ Hell heeft zijn prijs!
_werkwoord.
sap._ Als er iets is alles achter dit instinct is het waardevol om het daarna te traceren _accuraat_, dus ik kan maar beter beginnen, dus ... RM Renfield, datum 59 .-- Sanguinisch temperament; grote fysieke kracht; morbide prikkelbaar; perioden van somberheid, eindigend in een vast idee dat ik kan het niet bevatten.
Ik neem aan dat het sanguinische temperament zelf en de verontrustende invloed eindigt in een mentaal volbrachte finish; een mogelijk gevaarlijke man, waarschijnlijk gevaarlijk als onzelfzuchtig.
In zelfzuchtige mannen voorzichtigheid is net zo veilig een pantser voor hun vijanden als voor zichzelf.
Waar ik aan denk op dit punt is, wanneer zelf het vaste punt is dat de middelpuntzoekende kracht is uitgebalanceerd met de centrifugaal; wanneer plicht, een oorzaak, enz., de oplossing is punt, de laatste kracht is van het grootste belang, en alleen een ongeluk of een reeks van ongelukken kunnen het in evenwicht brengen.
_Letter, Quincey P. Morris tot Hon.
Arthur Holmwood... "_25 mei._ "Mijn beste kunst, - "We hebben garens verteld bij het kampvuur in de prairies en er eentje gekleed andermans wonden na het proberen van een landing op de Marquesas; en dronken gezondheid aan de oever van Titicaca.
