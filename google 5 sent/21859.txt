"Miss Eliza Bennet," zei Miss Bingley, "veracht kaarten.
Ze is geweldig lezer, en heeft geen plezier in iets anders. "
"Ik verdien zulk een lof en dergelijke censuur niet," riep Elizabeth; "Ik ben _not_ een geweldige lezer, en ik heb plezier in veel dingen. "
"Bij het verzorgen van je zuster weet je zeker dat je plezier hebt," zei Bingley; "en Ik hoop dat het snel zal toenemen door haar redelijk goed te zien. '
Elizabeth bedankte hem uit haar hart en liep toen naar de tafel waar een paar boeken leken.
