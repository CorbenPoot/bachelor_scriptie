In eerste instantie nam ik het voor het natte dak van een huis, maar de ene flits na de andere toonde het om in een snelle rollende beweging te zijn.
Het was een ongrijpbare visie - een moment van verbijsterende duisternis, en dan, in een flits zoals daglicht, het rood massa's van het weeshuis nabij de top van de heuvel, de groene toppen van de pijnbomen, en dit problematische object kwam duidelijk en scherp naar voren en helder.
En dit ding zag ik!
Hoe kan ik het beschrijven?
Een monsterlijk statief, hoger dan veel huizen, schrijdend over de jonge dennenbomen, en ze in de loop van hun carrière terzijde te schuiven; een lopende motor van glinstering metaal, nu over de heide schrijdend; articuleren touwen van staal eraan bungelend, en het ratelende tumult van zijn passage vermengt zich met de rel van de donder.
