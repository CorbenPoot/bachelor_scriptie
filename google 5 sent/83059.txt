Gelukkig, terwijl ik sprak mijn moedertaal, de heer Kirwin alleen begreep mij; maar mijn gebaren en bittere geschreeuw was voldoende om de andere getuigen angst aan te jagen.
Waarom ben ik niet dood gegaan?
Moeilijker dan de mens ooit tevoren was, waarom deed ik dat niet zinken in vergeetachtigheid en rust?
De dood grist veel bloemen weg kinderen, de enige hoop van hun toegewijde ouders; hoeveel bruiden en jeugdige minnaars zijn ooit een dag geweest in de bloei van gezondheid en hoop, en de daarna een prooi voor wormen en het verval van het graf!
Van welke materialen was ik maakte dat ik zo vele schokken kon weerstaan, die, zoals het keren van het wiel, voortdurend de marteling vernieuwd?
