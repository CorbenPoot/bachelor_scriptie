[Wordsworth's "Tintern Abbey".]
En waar bestaat hij nu?
Is dit zachte en mooie verloren gegaan Voor altijd  Heeft deze geest, zo vol van ideeën, fantasievolle verbeeldingen en prachtig, dat een wereld vormde, waarvan het bestaan ​​afhing van de leven van de schepper; is deze geest vergaan?
Bestaat het nu alleen in mijn geheugen?
Nee, het is niet zo; jouw vorm zo goddelijk bewerkt, en stralend van schoonheid, is vergaan, maar je geest bezoekt nog steeds en troost je ongelukkige vriend.
