Zelfs de dagbladen werden eindelijk wakker van de storingen, en populaire aantekeningen verschenen hier, daar en overal met betrekking tot de vulkanen op Mars.
Het seriocomische tijdschrift _Punch_, weet ik nog, maakte er een gelukkig gebruik van in de politieke cartoon.
En alles onvermoede, die raketten die de marsmannetjes op ons hadden geschoten, trokken naar de aarde, nu snellend met een snelheid van vele mijlen per seconde door de lege golf van ruimte, van uur tot uur en van dag tot dag, dichterbij en dichterbij.
Het lijkt me nu bijna ongelooflijk geweldig dat, met dat snel het lot hangt over ons heen, mannen kunnen hun kleine zorgen behandelen zoals zij deed.
Ik herinner me hoe jubelende Markham bezig was een nieuwe foto te bemachtigen van de planeet voor het geïllustreerde papier dat hij in die dagen had uitgegeven.
