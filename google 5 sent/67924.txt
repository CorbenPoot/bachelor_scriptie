Toen flapte hij eruit uit: "Blijf op de bergrug tot je bij de stomp van een den komt, draai dan naar links, voorbij de oude Raven Claim, en ga naar beneden volgen - en mag Gawd me helpen wanneer Simon terugkomt! "
"Bedankt," zei Esmeralda, net zo hoffelijk alsof ze in een Balzaal van Londen; en het volgende moment bleef de man staren na haar, nog steeds in een staat van vermengde verbijstering en bewondering.
Op de middag van die dag werden de goede mensen van Wally-Wally opgeschrikt door een man die in volle galop rijdt naar wat zijn marktplaats wordt genoemd.
Het paard was bedekt met zweet, besmeurd met schuim en hijgend alsof het net de Derby had gewonnen; de man was wit, bijna woedend, en zijn korte haar klampte zich vast aan zijn wenkbrauwen in transpirerende strepen.
Hij was bedekt met stof en zonder zijn hoed, want het was eraf gevallen ongeveer tien mijl terug en was genegeerd en overgelaten aan ornament het vliegtuig.
