Het hoge en besneeuwde bergen waren de directe grenzen, maar ik zag nee meer verwoeste kastelen en vruchtbare velden.
Onmetelijke gletsjers naderden de weg; Ik hoorde het gerommel van de vallende lawine en markeerde de rook van zijn passage.
Mont Blanc, de oppermachtige en prachtige Mont Blanc, verhief zich van de omringende _aiguilles_, en zijn enorme _dôme keek uit over de vallei.
Een tintelend, lang verloren gevoel van plezier kwam me vaak tegen reis.
Sommigen keren de weg in, een nieuw object wordt plotseling waargenomen en herkend, herinnerde me aan vervlogen dagen, en werd geassocieerd met de luchtige vrolijkheid van het jongensleven.
