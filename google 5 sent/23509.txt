Oh welke uren transport we zullen doorbrengen!
En wanneer we terugkeren, zal het niet zijn als een ander reizigers, zonder in staat te zijn een juist beeld van wat dan ook te geven.
Wo _ken_ weten waar we naartoe zijn gegaan - we zullen ons herinneren wat we hebben gezien.
Meren, bergen en rivieren zullen niet door elkaar worden gegooid in onze verbeelding; noch wanneer we een bepaalde scène proberen te beschrijven, zullen we beginnen te ruziën over zijn relatieve situatie.
Laat onze_ eerste effusies zijn minder ondraaglijk dan die van de algemeenheid van reizigers."
