Trouwens, Dorian, bedrieg niet Uzelf_______________________________________________________________________________5  Het leven wordt niet bepaald door wil of bedoeling.
Het leven is een kwestie van zenuwen en vezels, en langzaam opgebouwde cellen waarin gedachte verbergt zichzelf en passie heeft zijn dromen.
Misschien heb je zin in jezelf veilig en vind jezelf sterk.
Maar een toevallige toon van kleur in een kamer of een ochtendhemel, een bepaald parfum dat je ooit had geliefd en dat brengt subtiele herinneringen met zich mee, een lijn van een vergeten gedicht dat je opnieuw tegenkwam, een cadans van een muziekstuk dat je had opgehouden te spelen - ik zeg je, Dorian, dat het om dingen gaat zoals deze dat onze levens afhangen.
Browning schrijft daarover ergens; maar onze eigen zintuigen zullen ze ons voorstellen.
