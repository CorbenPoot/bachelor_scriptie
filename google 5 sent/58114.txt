"Ik hoop het," antwoordde meneer Knightley koel, en hervatte de brief.
“'Smallridge!wat doe je dan?
Wat is dit allemaal?"
'Ze was verloofd om als gouvernante naar de kinderen van mevrouw Smallridge te gaan - een beste vriend van mevrouw Elton - een buurman van Maple Grove; en, door de dag, vraag ik me af hoe mevrouw Elton de teleurstelling draagt? '
"Zeg niets, mijn lieve Emma, ​​terwijl je mij dwingt te lezen - zelfs niet van Mevrouw Elton.
