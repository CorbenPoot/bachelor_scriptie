#!/usr/bin/env python3

#This function splits the files of either google or sdl (whichever provided), into either 1, 5, 10 or 20 sentences (whichever provided), for the test or training data (whichever provided).
#The program then writes the output files to the corresponding folders.

#Use this program by first calling python3, then split.py, provide the number of sentences, specify whether to run on training or test data and 
#specify whether to run for the file containing all the translations by Google Translate or SDL Translate.
#Example: python3 split.py 20 training google

import sys
from nltk.tokenize import sent_tokenize

def main(argv):
	'''Splits a large text file into multiple text files'''

	no_of_sentences = argv[1] #Provided (via argv) number of sentences
	test_or_training = argv[2] #Provided test or train set
	google_or_sdl = argv[3] #Provided Google Translate or SDL Translate files

	try: #Depending on parameters described above, split and write to corresponding folder
		if test_or_training == "training" and google_or_sdl == "google":
			with open("google/total_google.txt", "r") as f:
				text = []
				for line in f:
					if line.strip() != "": #if line is not empty
						text.append(line.strip())
			sents = [sent.replace("\n", " ") for sent in sent_tokenize("\n".join(text))] #Tokenize sentence using the nltk sent_tokenize function

			text2 = []
			for i in range(len(sents)):
				text2.append(sents[i])
				if i % int(no_of_sentences) == int(no_of_sentences) - 1: #Check when the provided number of files is reached
					with open("{} {} {}{}{}".format("google", no_of_sentences, "sent/", i, ".txt"), "w") as f: #Declare file to be written to
						for line in text2:
							print(line, file=f) #Write to file
					text2 = []
			with open("{} {} {}{}{}".format("google", no_of_sentences, "sent/", i, ".txt"), "w") as f: 
				for line in text2:
					print(line, file=f)		

		elif test_or_training == "test" and google_or_sdl == "google":
			with open("test google/total_google_test.txt", "r") as f:
				text = []
				for line in f:
					if line.strip() != "":
						text.append(line.strip())
			sents = [sent.replace("\n", " ") for sent in sent_tokenize("\n".join(text))]

			text2 = []
			for i in range(len(sents)):
				text2.append(sents[i])
				if i % int(no_of_sentences) == int(no_of_sentences) - 1:
					with open("{} {} {}{}{}".format("test google", no_of_sentences, "sent/", i, ".txt"), "w") as f: 
						for line in text2:
							print(line, file=f)
					text2 = []
			with open("{} {} {}{}{}".format("test google", no_of_sentences, "sent/", i, ".txt"), "w") as f: 
				for line in text2:
					print(line, file=f)

		elif test_or_training == "training" and google_or_sdl == "sdl":
			with open("sdl/total_sdl.txt", "r") as f:
				text = []
				for line in f:
					if line.strip() != "":
						text.append(line.strip())
			sents = [sent.replace("\n", " ") for sent in sent_tokenize("\n".join(text))]

			text2 = []
			for i in range(len(sents)):
				text2.append(sents[i])
				if i % int(no_of_sentences) == int(no_of_sentences) - 1:
					with open("{} {} {}{}{}".format("sdl", no_of_sentences, "sent/", i, ".txt"), "w") as f:
						for line in text2:
							print(line, file=f)
					text2 = []
			with open("{} {} {}{}{}".format("sdl", no_of_sentences, "sent/", i, ".txt"), "w") as f: 
				for line in text2:
					print(line, file=f)		

		elif test_or_training == "test" and google_or_sdl == "sdl":
			with open("test sdl/total_sdl_test.txt", "r") as f:
				text = []
				for line in f:
					if line.strip() != "":
						text.append(line.strip())
			sents = [sent.replace("\n", " ") for sent in sent_tokenize("\n".join(text))]

			text2 = []
			for i in range(len(sents)):
				text2.append(sents[i])
				if i % int(no_of_sentences) == int(no_of_sentences) - 1:
					with open("{} {} {}{}{}".format("test sdl", no_of_sentences, "sent/", i, ".txt"), "w") as f:
						for line in text2:
							print(line, file=f)
					text2 = []
			with open("{} {} {}{}{}".format("test sdl", no_of_sentences, "sent/", i, ".txt"), "w") as f: 
				for line in text2:
					print(line, file=f)

	except NameError: #When a wrong word is presented as argv show the following error
		print("{} \n {}".format("Please input the right paramaters", "Example: python3 split.py 10 test sdl"))


if __name__ == "__main__":
	main(sys.argv)