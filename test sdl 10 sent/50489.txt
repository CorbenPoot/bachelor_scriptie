De standpunten van de twee essays in bijna rechtstreeks in tegenspraak.
Bij het lezen, Alexander Bain's _The Art studie_ in zijn _Praktische Essays_, zullen worden gevonden.
Bacon's essay _op studies_, dat is niet meer dan een paar bladzijden, bevat meer geconcentreerde wijsheid dan is te vinden.
Over onderwerpen de meeste gedachten, de lezer niet beter doen dan lezen Herbert Spencer's essay _welke kennis is het meest waard?_ in zijn _Onderwijs_.
Als boeken meest lezen waard, raadpleeg de lijsten van John Morley, Sir John Lubbock, en Frederic Harrison; Sonnenschein _Beste boeken_ (in twee delen); Baldwin's _Het Boek Lover_; Dr. Eliot's _vijf voet plat_ en Frank Parson's _'s werelds beste boeken_, eerder naar verwees.
Over de kunst van het leven en de kunst van het plannen van tijd tot gevolg hebben dat de ruimte voor denken, evenals waardevolle tips over hoe deze manier van denken wordt verricht-consult Arnold Bennett, _How to live op vierentwintig uur per dag_ en E. H. Griggs, _Het gebruik van de marge_ (beide zeer, zeer kleine oplages).
Tot slot is er veel nuttig materiaal, evenals een onschatbare bron van inspiratie zal worden verkregen uit de intellectuele en literaire biografieën van grote denkers.
Dit is vooral het geval bij autobiografie.
O.a.
kan worden genoemd de hun autobiografie geschreven van John Stuart Mill en Herbert Spencer en autobiografisch fragment door Charles Darwin.
