Het verhaal zit vol met slimme en heldere communicatie, en dat is eigenlijk één van absurde humor, de plot is boeiend en zelfs sensationeel.
De Boerderij dienaar door E. H. ANSTRUTHER het centrale thema van dit opmerkelijke debuutroman is de passionele liefde-verhaal van Anna Murrell, "De Boerderij dienaar."
Maar de studie van Frank Harding en de wijze waarop zijn leven werd beïnvloed door drie vrouwen is van even groot belang.
Enkele moderne romans zijn zo gevarieerd, voor de twee grootste delen van het boek behandel een rustig dorpje in East Anglia en met het Quartier Latin in Parijs vlak voor de oorlog.
Het is een boek met passages van grote dramatische kracht; maar deze worden afgewisseld met hoofdstukken heerlijk voor hun stille humor en scherpe observaties van de kleine "Actie en reactie" van het dagelijks leven.
Het is een lang boek: maar alleen in een lange boek kon de auteur, naast werken uit het hoofdthema , ertoe hebben geleid dat een dergelijk groot aantal bijfiguren.
Mr. Harding, de Carringtons, de Juleses, Dr. Emmersley, Lucie Dubels en een dozijn anderen zijn allemaal zo echt als de mensen je ontmoet elke dag.
De lange echtscheiding (John Bogardus) door George A. CHAMBERLAIN Auteur van "door glas-in-lood", "Thuis", enz.
andere briljante, boeiend, kant-en-gewone Chamberlain roman.
Het panoramische uitzicht is Europa, Amerika, Afrika, en de zeeën tussen.
