A. BILLY in _Parijs Midi_, feb 9, 1916:- Deze pagina's zijn het dagboek van de man die, onder alle Franse gevangenen is misschien het best te begrijpen vanuit Duitsland binnen.
_La Tribuna_, Feb 20 1916:- Hoewel geen roman, het is zo fascinerend als een roman.
DANIEL LESUEUR in _La Renaissance_, 18 Maart 1916:- Iedereen moet deze inventaris van gevangenisstraf, wiens realisme-simple, triviaal, en soms bijna weerzinwekkende-wordt bestraald met een schoonheid die geen werk van romantische fictie kan ooit gelijk.
MARCEL ROUFF in _Mercure de France_, 1 April 1916:- Het boek zal winnen door te lezen en herlezen na de oorlog, toen de vrede zal hebben hersteld en dat de onafhankelijkheid van geest die noodzakelijk is voor een adequate beoordeling van kunstwerken.
PAUL BOURGET in _Echo de Paris_ 28 April 1916:- Ik vind het _journal d'un simple sprake_, één van de beste voorbeelden van de literatuur van oorlog impressies die kenmerkend is voor het huidige conflict bezig.… Het boek is zo boeiend als een roman en als levende geschiedenis.
Het dagboek van een Franse particuliere WAR-gevangenisstraf 1914-1915 Door GASTON RIOU Vertaald uit het Frans door EDEN EN CEDER PAUL [Afbeelding] LONDON: GEORGE ALLEN & UNWIN LTD. RUSKIN HOUSE 40 MUSEUMSTRAAT, toilet.
_First published in 1916_ (_All rights reserved_) biografische opmerking _Gaston Riou werd geboren op 7 januari 1883.
Hij is een inwoner van de Cévennes, de regio die zijn afgeleid van drie van de meest vooraanstaande onder de moderne Franse psychologen, Melchior de Vogüé, Auguste Sabatier, Paul Bourget.
De Cévenole familie waarvan hij veren een actieve rol gespeeld in de oorlogen van de religie.
Over de moeder van de zijde hij is gerelateerd aan Jacques de Vaucanson, de toonaangevende Franse mechanical engineer van de achttiende eeuw, en om Désubas Majal, de laatste Hugenoten martelaar, terechtgesteld op Montpellier in 1747.
