"Nou, laten we gaan!
En nu gaan we hand in hand gaan."
"En altijd al ons leven hand in hand!
Lang leve Karamazov!"
Kolya huilde nogmaals Rapturously received, And once more boys zijn Uitroep: "lang leve Karamazov!"
Het einde Het dagboek van een Franse particuliere enkele REVIEWS VAN DE FRANSE EDITIE EMILE FAGUET in _Les Annales Politiques et Littéraires_, 5 Maart 1916:- Ik heb de eer gehad … drie jaar geleden tot het schrijven van het voorwoord bij M. Gaston Riou's eerste boek, _Aux écoutes de la France qui vient_.
Het zat vol met vuur, elan en passie; het was een hart-beat.
Ik was niet altijd dezelfde mening toegedaan als de auteur, maar ik heb nooit niet met hem eens.
Ik voelde me in hem tegelijk een broer in patriottisme en broer verliefd op waarheid en rechtvaardigheid.
Ik groette hem liefdevol en hem teder.
