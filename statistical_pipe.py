#!/usr/bin/env python3

#This function balances the google and sdl data, divides it into 100 parts and
#returns the averaged F-score of the used labels for all seperate parts.

#Use this program by first calling python3, then statistical_pipe.py, provide the number of sentences, specify whether to run on training or test data and 
#specify whether to run for the POS-tag files or the text files.
#Example: python3 statistical_pipe.py training text

import sys
import glob
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import f1_score

np.random.seed(0) #Makes the random numbers predictable so that each time the same intances are used

def classifier(files_google, files_sdl):
	'''This function enables the classification of given labeled lists of text or POS-tag files'''

	y = np.array(["google"]*len(files_google) + ["sdl"]*len(files_sdl)) #Create labels for classification and insert in numpy array
	X = np.array(files_google + files_sdl) #Put the files in a numpy array
	
	folder = StratifiedKFold(n_splits = 10, shuffle=True) #Create a stratified 10 fold cross validation

	y_total_test = np.array([])
	y_total_observed = np.array([])

	#Pipeline used for vectorization and classification
	pipeline = Pipeline([("vect", TfidfVectorizer(input="filename", binary=False)), ("clf", LinearSVC())])

	
	for train_i, test_i in folder.split(X, y):
		pipeline.fit(X[train_i], y[train_i]) #Train the model

		y_obs = pipeline.predict(X[test_i]) #Let the model predict and thus classify
										#	
		y_total_test = np.concatenate([y_total_test, y[test_i]])
		y_total_observed = np.concatenate([y_total_observed, y_obs])

	#Returns classifier algorithm name with a comma, followed by the F-score so that it is already in a CSV format.
	print("{}{}".format("LinearSVC,",round(f1_score(y_total_test, y_total_observed, average="weighted"),2))) 


def main(argv):
	test_or_training = argv[1] #Provided test or train set
	text_or_pos = argv[2] #Provided textual or POS-tag data
	if text_or_pos == "text":
		text_or_pos = "txt"
	try:
		#Below the files are gathered from either the training or test data (no. of sentences provided by user), and depending on the user's input, whether textual or POS-tag data is gathered
		if test_or_training == "training" and text_or_pos == "pos":
			google = "{} {} {} {}{}{}".format("google", "1", "sent", text_or_pos, "/*.", text_or_pos)
			sdl = "{} {} {} {}{}{}".format("sdl", "1", "sent", text_or_pos, "/*.", text_or_pos)
		elif test_or_training == "training" and text_or_pos == "txt":
			google = "{} {} {}{}{}".format("google", "1", "sent", "/*.", text_or_pos)
			sdl = "{} {} {}{}{}".format("sdl", "1", "sent", "/*.", text_or_pos)
		elif test_or_training == "test" and text_or_pos == "pos":	
			google = "{} {} {} {}{}{}".format("test google", "1", "sent", text_or_pos, "/*.", text_or_pos)
			sdl = "{} {} {} {}{}{}".format("test sdl", "1", "sent", text_or_pos, "/*.", text_or_pos)
		elif test_or_training == "test" and text_or_pos == "txt":	
			google = "{} {} {}{}{}".format("test google", "1", "sent", "/*.", text_or_pos)
			sdl = "{} {} {}{}{}".format("test sdl", "1", "sent", "/*.", text_or_pos)

		files_google = glob.glob(google)[:91000] #Look through the google directory described above and put files in a list, balance at 91000 files
		files_sdl = glob.glob(sdl)[:91000] #Look through the sdl directory described above and put files in a list, balance at 91000 files

		google_list = []
		sdl_list = []
		count = 1

		#Loop that takes one document per label, appends it to their respective lists, passes the lists through to the classifier function when 100 items are appended
		for i,j in zip(files_google, files_sdl):  
			google_list.append(i)
			sdl_list.append(j)
			if count == 910:
				count = 0
				classifier(google_list, sdl_list)
				google_list = []
				sdl_list = []
			count += 1

	except NameError: #When a wrong word is presented as argv show the following error
		print("{} \n {}".format("Please input the right paramaters", "Example: python3 statistical_pipe.py 20 training text"))


if __name__ == "__main__":
	main(sys.argv)