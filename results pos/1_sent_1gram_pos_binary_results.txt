	google				sdl


	-0.5153	jjs            		0.7664	pdt            
	-0.3078	ls             		0.4040	rp             
	-0.2707	sym            		0.3219	to             
	-0.2152	rbr            		0.0950	jj             
	-0.2139	nnps           		0.0674	dt             
	-0.1154	vbz            		0.0441	wrb            
	-0.1064	jjr            		0.0355	nns            
	-0.1044	pos            		0.0247	md             
	-0.0989	prp            		0.0221	wdt            
	-0.0934	cc             		0.0130	vbg            


             precision    recall  f1-score   support

     google       0.52      0.39      0.45     91155
        sdl       0.52      0.65      0.58     93581

avg / total       0.52      0.52      0.52    184736

