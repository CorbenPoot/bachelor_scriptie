	google				sdl


	-5.2462	pos nnp        		3.0720	rp             
	-3.1955	uh nn          		2.4882	jj uh          
	-2.6418	vbd jjs        		2.0733	jj uh nnp      
	-2.6285	pos nnp nn     		1.9422	prp            
	-2.3288	nnp nn nn jj   		1.7230	vbz            
	-2.3166	jjs            		1.6624	prp vbp nn vbp 
	-2.1345	nnp nn nn nn nn		1.6471	prp vbg        
	-1.9978	nn nnp vbd cd  		1.6275	to             
	-1.9815	uh nn nn       		1.5983	cd nnp pos nnp 
	-1.9348	nn nn nn nn vbd		1.5959	nnp nnp pos nn nn


             precision    recall  f1-score   support

     google       0.63      0.62      0.63     18231
        sdl       0.64      0.65      0.64     18717

avg / total       0.64      0.64      0.64     36948

