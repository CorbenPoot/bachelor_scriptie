	google				sdl


	-1.0193	jjs            		0.6756	nns            
	-0.5092	fw             		0.3807	rp             
	-0.4801	sym            		0.3738	pdt            
	-0.4627	jj             		0.3645	ls             
	-0.4627	nn             		0.3126	to             
	-0.4627	nnp            		0.1142	vbg            
	-0.4510	nnps           		0.0328	rb             
	-0.3729	rbr            		0.0171	vbp            
	-0.2750	jjr            		-0.0119	in             
	-0.2515	pos            		-0.0763	wrb            


             precision    recall  f1-score   support

     google       0.60      0.49      0.54      9116
        sdl       0.58      0.68      0.63      9359

avg / total       0.59      0.59      0.58     18475

