	google				sdl


	-4.0539	jjs            		3.4220	rp             
	-3.8326	pos nnp        		2.6206	jj uh          
	-3.5708	in fw nns vbd  		2.4702	nn prp jj nnp  
	-3.1226	uh nn          		2.4391	prp vbp nn vbp 
	-2.8848	cd cd jj nnp   		2.3960	nns nn vbd vbn 
	-2.7584	fw nns vbd vbn 		2.2701	jj uh nnp      
	-2.7512	vbd jjs        		2.0675	cd nnp pos nnp 
	-2.7253	nn nnp vbd cd  		2.0610	to             
	-2.3237	nns nn jj nnp  		2.0092	nns nnp vbz nn 
	-2.2580	nn nn fw cd    		1.9591	prp            


             precision    recall  f1-score   support

     google       0.66      0.67      0.66     18231
        sdl       0.67      0.66      0.67     18717

avg / total       0.66      0.66      0.66     36948

