             precision    recall  f1-score   support

     google       0.54      0.51      0.52     91155
        sdl       0.54      0.57      0.56     93581

avg / total       0.54      0.54      0.54    184736