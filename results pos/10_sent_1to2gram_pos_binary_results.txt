	google				sdl


	-3.6030	pos nnp        		1.7660	dt prp         
	-2.6917	vbd jjs        		1.7553	jj dt          
	-2.3075	uh nn          		1.6632	uh prp         
	-2.0813	nn sym         		1.6032	jj uh          
	-1.9689	uh sym         		1.5790	vbp rp         
	-1.9008	vbg jjs        		1.5558	dt vbp         
	-1.6892	sym uh         		1.5071	prp vbg        
	-1.6565	wdt nn         		1.5055	pos nn         
	-1.6374	sym vbz        		1.4206	dt vbn         
	-1.6326	cd nnp         		1.4192	sym            


             precision    recall  f1-score   support

     google       0.69      0.65      0.67      9116
        sdl       0.68      0.72      0.70      9359

avg / total       0.68      0.68      0.68     18475

