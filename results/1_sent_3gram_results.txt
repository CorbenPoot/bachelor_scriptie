             precision    recall  f1-score   support

     google       0.58      0.51      0.54     91155
        sdl       0.57      0.64      0.60     93581

avg / total       0.58      0.58      0.57    184736