             precision    recall  f1-score   support

     google       0.81      0.80      0.80      9116
        sdl       0.81      0.81      0.81      9359

avg / total       0.81      0.81      0.81     18475