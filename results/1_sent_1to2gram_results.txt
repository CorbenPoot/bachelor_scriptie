             precision    recall  f1-score   support

     google       0.75      0.74      0.74     91155
        sdl       0.75      0.76      0.75     93581

avg / total       0.75      0.75      0.75    184736