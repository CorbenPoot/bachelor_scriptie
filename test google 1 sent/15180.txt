En hij hield zijn woord; hij stierf en liet alles over aan zijn zonen, die met hun vrouwen en kinderen, hij had zijn hele leven als bedienden behandeld.
