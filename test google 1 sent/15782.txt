Hij stopte ook niet op de treden, maar ging snel naar beneden; zijn ziel, vol van vervoering, verlangde naar vrijheid, ruimte, openheid.
