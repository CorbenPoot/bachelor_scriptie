Delen zonder te denken aan de dag van morgen, om te leven zonder berekening, om te handelen alleen zoals het hart dicteerde - het was als het paradijs.
