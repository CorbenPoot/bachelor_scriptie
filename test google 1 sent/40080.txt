De man die grijs met zwart vergelijkt, wordt grijs-wit genoemd; en de man die vergelijkt afwezigheid van pijn met pijn, noemt de afwezigheid van pijnplezier.
