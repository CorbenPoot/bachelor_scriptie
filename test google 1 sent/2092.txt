Smeek en bid zoals de muis en de worst misschien, het had geen zin; de vogel bleef meester van de situatie, en de onderneming moest worden gemaakt.
