Hij gaat terug, moe van de schepping die hij heeft opgestapeld om hem voor zichzelf te verbergen, een oude hond die een oude pijn likt.
