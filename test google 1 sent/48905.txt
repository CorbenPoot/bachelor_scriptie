Ons vlak van denken is bepaald niet alleen door de goede boeken die we lezen, maar door alle boeken die we lezen; het neigt naar het gemiddelde_.
