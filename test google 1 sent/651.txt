"Als hij ons gewoon kon begrijpen", zei zijn vader bijna als een vraag; zijn zuster schudde haar hand krachtig door haar tranen heen als een teken dat daar geen sprake van was.
