Het maakt je denken veel minder vaag dan als je stil dacht, je woordenschat verhoogt, altijd bewaart tempo met uw ideeën, en vereist vrijwel geen aandacht.
