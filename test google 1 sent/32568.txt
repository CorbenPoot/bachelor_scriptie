Het het weer was prachtig; de zon verspreidde geleidelijk het transparante dampen; en toch kon men zich voorstellen dat de hele natuur huilde.
