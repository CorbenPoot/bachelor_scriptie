Geen van de genaden van een mooie ziel ontbreekt in hem; evenmin kan hij de dood vrezen, of veel denken aan het menselijk leven.
