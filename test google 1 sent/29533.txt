Twee bij twee, zoals de gewoonte is in Duitse barakken, begeven we ons naar de keuken - een lange optocht van individuen die ongeduldig babbelen in het donker en slecht ruikende passages.
