Van het huis van mijn jeugd heb ik niets dan kostbaars meegebracht herinneringen, want er zijn geen herinneringen die kostbaarder zijn dan die van vroeger kindertijd in iemands eerste huis.
