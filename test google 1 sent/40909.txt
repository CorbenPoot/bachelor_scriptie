Onderwijs wordt door hem vertegenwoordigd, niet als de vulling van een schip, maar als het draaien van het oog van de ziel naar het licht.
