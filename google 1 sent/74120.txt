En 's avonds kwamen veel mensen haastend langs de weg in de buurt van hun halte, op de vlucht voor onbekende gevaren voor hen, en in de richting gaan waaruit mijn broer was gekomen.
