Edward II gaf Piers Gaveston een pak rood-gouden harnas bezaaid met jacinths, een kraag van gouden rozen gezet met turquoise-stenen, en een skull-cap _parseme_ met parels.
