Met kaarsen en fakkels van dennenhout in hun handen gingen ze de ronde van tent tot tent, hut tot hut; elke gereedschapsschuur, elke centimeter van dekking werd nauwkeurig onderzocht.
