Soms toen de natuur, overweldigd door honger, zonk onder de uitputting, een maaltijd was voorbereid op mij in de woestijn die me herstelde en inspireerde.
