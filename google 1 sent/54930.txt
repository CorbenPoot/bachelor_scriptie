Alles wat teder en charmant was, was om hun te markeren afscheid; maar toch moesten ze afscheid nemen.
