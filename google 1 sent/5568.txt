En de derde keer dat ik wakker werd de dageraad, en gleed naar beneden, en ze was er al, en haar kaars was het meest uit, en haar oude grijze hoofd rustte op haar hand en zij sliep.
