Vaders en moeders die het hadden vol gehad deel in het ergste van de dag, zachtjes gespeeld met hun magere kinderen; en geliefden, met zo'n wereld om hen heen en voor hen, geliefd en gehoopt.
