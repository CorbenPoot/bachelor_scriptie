De dag naderde, de dag is aangebroken; en na een ochtend van wat angstig kijken, Frank Churchill bereikte, in alle zekerheid van zichzelf, Randalls eerder avondeten en alles was veilig.
