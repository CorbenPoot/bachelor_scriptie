Maar toch zien we elke dag om ons heen de groei van nieuwe overtuigingen, die zichzelf nieuw vinden; en die nog maar het oude zijn, die doen alsof om jong te zijn - zoals de fijne dames in de opera.
