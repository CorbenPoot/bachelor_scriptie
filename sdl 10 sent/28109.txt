"eigenaardige--dat is het woord," zei Holmes.
"Het is geen Engels papier.
Houd hem tot het licht."
Ik deed dat en zag een grote "E" met een kleine "g", een "P" en een grote "G" met een kleine "t" woven in de textuur van het papier.
"Wat vindt u daarvan?"
vroeg Holmes.
"De naam van de maker, No Doubt; of zijn monogram, nogal."
"helemaal niet.
De 'G' met de kleine 't' staat voor 'Gesellschaft', de Duitse 'bedrijf.'
Het is een gebruikelijke samentrekking als onze 'Co.'
