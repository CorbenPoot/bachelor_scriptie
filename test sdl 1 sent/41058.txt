Nogmaals, plezier en pijn zijn bewegingen, en het ontbreken daarvan is rust; maar als dat zo is, hoe kan de afwezigheid van een van hen worden?
