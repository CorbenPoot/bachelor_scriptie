Ondertussen, in hun eenzaamheid, houden het beeld van Christus rechtvaardig en onbezoedeld, in de zuiverheid van Gods waarheid, uit de tijd van de vaderen van het oude, de apostelen en de martelaren.
