Het grove sterkte van een militaire regering was niet gunstig voor zuiverheid en raffinement; en de buitensporige striktheid van sommige verordeningen lijkt te hebben geleid tot een reactie.
