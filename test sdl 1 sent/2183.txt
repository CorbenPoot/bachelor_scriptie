Smeken en bidden als de muis en de worst, het was geen gebruik; de vogel bleef meester van de situatie en de onderneming moesten worden gemaakt.
