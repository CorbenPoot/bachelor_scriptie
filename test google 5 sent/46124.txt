Ik zal het uitleggen: het lichaam dat groot is wanneer het in de buurt wordt gezien, lijkt klein wanneer gezien op afstand?
waar En hetzelfde object lijkt recht uit het water te kijken, en krom in het water; en de concave wordt convex, ten gevolge van de illusie over kleuren waarvoor de aanblik aansprakelijk is.
Dus van elke soort van verwarring wordt in ons geopenbaard; en dit is die zwakte van de menselijke geest waarop de kunst van het oproepen en bedriegen door licht en schaduw en andere ingenieuze apparaten leggen op, met een effect op ons als magie.
waar En de kunst van het meten en nummeren en wegen komt tot de redding van het menselijk begrip - er is de schoonheid van hen - en de schijnbaar groter of kleiner, of meer of zwaarder, hebben niet langer het meesterschap over ons heen, maar wijken voor berekening en maat en gewicht?
Meest waar.
