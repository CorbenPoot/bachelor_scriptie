Tot dit bezwaar heb ik niets te zeggen, want ik beperk mijn opmerkingen voor hen die op zoek zijn naar waarheid en kennis in plaats van conversatie en de goede mening van degenen die geloven dat het lezen van dekking tot dekking is het enige pad naar wijsheid.
Ik kan echter terloops wijzen op dat als we deze methode volgen, zullen er een half dozijn boeken zijn die wij kan zeggen dat we "doorgekeken" hebben naar een die we anders zouden hebben kunnen zeggen dat we "gelezen" hadden.
Deze manier van omgaan met een boek is constructief en positief in tegenstelling tot de negatieve methode van kritisch lezen.
Want we lezen voor suggestie enkel en alleen; we brengen een bepaalde gedachtegang van een auteur voort, dat is beter voor intellectuele ontwikkeling dan proberen te achterhalen of hij dat wel was verkeerd en waar hij ongelijk had.
Niet alleen is deze positieve methode meer interessant; in sommige opzichten is het zelfs beter voor kritiek.
