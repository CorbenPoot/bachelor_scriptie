Wat voor kwaad?
Rijkdom, zei ik, en armoede; de een is de ouder van luxe en indolentie, en de andere van gemeenheid en wreedheid, en beide ontevredenheid.
Dat is heel waar, antwoordde hij; maar toch zou ik graag willen weten, Socrates, hoe onze stad in staat zal zijn om oorlog te voeren, vooral tegen een vijand die rijk en machtig is, indien verstoken van de pezen van oorlog.
Er zou zeker een probleem zijn, antwoordde ik, om oorlog mee te voeren een dergelijke vijand; maar er is geen probleem waar er twee van zijn.
- Hoezo?
