S S. Colvin's, _The Learning Process_ bevat enkele interessante hoofdstukken denken.
Over de methode is de hoeveelheid literatuur nog indrukwekkender dan dat over de psychologie van redeneren.
Waarschijnlijk het meest grondige boek is Stanley Jevon's _The Principles of Science', hoewel dit, bestaande van twee volumes, vergt nogal wat ambitie om aan te vallen.
Een goede recent kort werk is JA Thomson, _Inleiding tot Science_.
Herbert Spencer's korte essay, _An Element in Method_, in zijn _Various Fragments_ kan ook worden genoemd.
