Het is een weg geworden.
Hoewel ik een sociaal wezen ben en plezier heb in gezelschap, vind ik het wel buitengewoon plezierig om alleen te zijn.
Ik heb lange uren nodig voor mezelf.
In Parijs, in Dully, in Lablachère, ben ik nooit moe van mijn werkkamer, waar ik zie niemand vóór de lunch.
De ochtenden zijn altijd te kort.
