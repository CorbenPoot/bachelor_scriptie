Ik luister naar je en ben achtervolgd door een droom .... Het is een droom die ik soms heb, weet je ... Ik droom er vaak van - het is altijd hetzelfde ... dat iemand op me jaagt, iemand waar ik verschrikkelijk bang voor ben ... dat hij jaagt op me in het donker, in de nacht ... op mij, en ik verstop me ergens achter hem, achter een deur of kast, verstop je op een vernederende manier, en het ergste is dat hij altijd weet waar ik ben, maar hij doet alsof hij dat niet doet weet waar ik expres ben, om mijn lijdensweg te verlengen, om van mijn angst te genieten .... Dat is precies wat je nu doet.
Het is niet anders!"
"Is dat het soort dingen waar je over droomt?"
vroeg de officier van justitie.
"Dat is het wel!
