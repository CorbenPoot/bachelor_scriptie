Zoals de naam al aangeeft, het tracht een _norm_ of patroon vast te stellen dat moet worden aangehouden.
Het bestudeert manieren om gewenste doelen te bereiken.
Tot deze klasse behoren zulke wetenschappen als ethiek, onderwijs, landbouw.
Nu zijn deze normatieve wetenschappen, met uitzondering van de ethiek, bijna wordt altijd verwezen naar 'kunst' of 'toegepaste wetenschappen'.
Aan beide van deze termen heb ik technisch maar krachtig bezwaar.
