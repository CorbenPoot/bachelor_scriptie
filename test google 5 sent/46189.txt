Nee, zei hij, dat is zeker niet redelijk.
Neen, zei ik, redelijk vanuit één oogpunt.
Welk standpunt?
Als je bedenkt, zei ik, dat we ons bij een ongeluk natuurlijk voelen honger en verlangen om ons verdriet te verlichten door geween en geweeklaag, en dat dit gevoel dat in onze eigen rampen onder controle wordt gehouden, is tevreden en blij door de dichters; - de betere natuur in elk van ons, niet voldoende getraind door reden of gewoonte, staat het toe sympathiek element om los te breken omdat het verdriet anders is; en de toeschouwer fantaseert dat er geen schande voor hemzelf in kan zijn lof en medelijden met iemand die komt hem vertellen wat een goede man hij is, en het maken van een ophef over zijn problemen; hij denkt dat het plezier is een winst, en waarom zou hij hooghartig zijn en dit en het gedicht verliezen te?
Weinig mensen reflecteren, zoals ik zou moeten veronderstellen, van het kwaad van andere mensen wordt iets van het kwaad aan zichzelf doorgegeven.
