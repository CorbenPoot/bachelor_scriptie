Zoals een schrijver het uitdrukt: "We vormen vaak onze mening over het geringste bewijs, maar we zijn geneigd om ons daaraan vast te klampen ze met grimmige vasthoudendheid. "
Er zijn hiervoor twee redenen.
Als we ergens een mening over hebben gevormd, is de kans groot dat we dat doen hebben dit aan iemand meegedeeld en hebben ons daarbij verbonden aan die kant.
Een mening omdraaien is bekennen dat we dat waren eerder fout.
Een mening terugdraaien is om ons open te stellen voor de lading van inconsistentie.
