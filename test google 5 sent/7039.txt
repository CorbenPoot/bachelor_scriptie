Dit is in 1983. mijn laatste boodschap aan jou: zoek in verdriet naar geluk.
Werk, werk onophoudelijk.
Onthoud mijn woorden, want hoewel ik weer met je zal praten, niet alleen mijn dagen maar mijn uren zijn geteld. "
Alyosha's gezicht verried opnieuw sterke emotie.
De hoeken van zijn mond trilden.
