De lijn kan worden beschouwd als reikend van eenheid tot oneindig, en is verdeeld in twee ongelijke delen, en onderverdeeld in twee meer; elke lagere bol is de vermenigvuldiging van het voorgaande.
Van de vier faculteiten, het geloof in de lagere divisie heeft een tussenpositie (cp.
voor het gebruik van het woord geloof of overtuiging, (Grieks), Timaeus), contrasterend evenzeer met de vaagheid van de waarneming van schaduwen (Grieks) en de hogere mate van zekerheid (Grieks) en reden (Grieks).
Het verschil tussen begrijpen en denken of reden (Grieks) is analoog aan het verschil tussen het verwerven van kennis in de delen en de contemplatie van het geheel.
Ware kennis is een geheel en is dat ook onbeweeglijk; consistentie en universaliteit zijn de tests van de waarheid.
