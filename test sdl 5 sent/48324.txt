We zijn nu klaar om een aantal constructieve methoden in het denken.
Eén methode voor bijna alle problemen is wat wij noemen de _deductieve_ of _à priori_ methode.
Deze methode houdt een conclusie zonder waarneming of experiment.
Het bestaat uit het redeneren vanuit eerdere ervaringen of van gevestigde beginselen aan bepaalde feiten.
Het kan echter worden gebruikt om te bevestigen observatie en experiment evenals hun plaats.
