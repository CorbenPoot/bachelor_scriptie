Wat de werkwijze betreft, de hoeveelheid literatuur is nog meer opleggen dan die op de psychologie van redeneren.
Waarschijnlijk de meest grondige boek is Stanley Jevon's _De principes van de wetenschap_, hoewel dit bestaat uit twee volumes, vereisen heel wat ambitie voor een aanval.
Een goede recente korte werk J.
A. Thomson, _Inleiding tot de wetenschap_.
Herbert Spencer's korte essay, _een element in de methode_ in zijn _verschillende fragmenten_ kan ook worden vermeld.
