Guy Thorne's new novel, "wanneer de gemene Man …", is een ongewoon en indringend.
Het is een grondige studie van een slechte man's soul, stripped bare en naakt zonder uitvluchten of belastingontduiking.
Als Maupassant geschreven had dit verhaal zou hebben gestopt bij het derde boek, en een briljante maar onuitsprekelijk pijnlijke document zouden allen die zijn gebleven.
De heer Guy Thorne gaat verder.
Hij toont ons de donkere en sensuele ziel van Sebastian warde op weg naar het Licht, totdat de goddeloze man eindelijk zich van zijn goddeloosheid en, teruggedrukt, gebroken en leeg is, werpt zich aan de voeten van God.
