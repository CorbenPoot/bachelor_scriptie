Voor het grootste deel van menselijke handelingen zijn noch opportuun noch verkeerd, behalve voor zover zij strekken tot het geluk van de mensheid (Introd.
Gorgias en Philebus).
Dezelfde vraag terugkomt in de politiek, waar het nuttig of wenselijk lijkt om aanspraak te maken op een groter gebied en meer autoriteit.
Ten aanzien van politieke maatregelen hebben we voornamelijk vragen: Wat is de invloed daarvan op het geluk van de mensheid?
Maar ook hier kunnen we vaststellen dat wat wij opportuniteit slechts het recht van de rechter beperkt wordt door de voorschriften van de menselijke samenleving.
