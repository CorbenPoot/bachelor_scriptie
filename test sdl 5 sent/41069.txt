En een soortgelijke verwarring gebeurt met plezier en pijn, en vele andere dingen.
De man die vergelijkt grijs met zwart, gesprekken grijs wit; en de man die vergelijkt het ontbreken van pijn met pijn, roept het ontbreken van pijn plezier.
Nogmaals, honger en dorst zijn inanitions van het lichaam, de onwetendheid en de dwaasheid van de ziel; en het eten is de tevredenheid van de ene, kennis van de ander.
Nu is het puur genoegen -- die van eten en drinken, of van kennis?
Wij beschouwen de zaak aldus: De tevredenheid van meer bestaan is mooier dan die minder.
