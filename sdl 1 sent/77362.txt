De noordelijke heuvels waren gehuld in duisternis; het vuur in de buurt van Kensington glowed redly, en nu en dan een oranje-rode tong van vuur flitste op en verdwenen in de diepe blauwe nacht.
