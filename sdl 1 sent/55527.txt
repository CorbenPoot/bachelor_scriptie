Men kan kruipen bij een reis; men kan plaatsnemen op sneller dan men denkt; en het genot van herkomst bij de vrienden voordat de look-out begint, is zeker veel meer dan een beetje inspanning nodig."
