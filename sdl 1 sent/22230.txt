Maar tot openheid zonder met weinig uiterlijk vertoon of design--Het goede van ieders karakter en nog beter, en dan heb ik het nog niet eens over de slechte--behoort tot je alleen.
