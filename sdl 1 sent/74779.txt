Een paar minuten voordat er nog maar drie echte dingen voor mij--de onmetelijkheid van de nacht en ruimte en natuur, mijn eigen ongelofelijke lafheid en angst, en de nabije benadering van de dood.
